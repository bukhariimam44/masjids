<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriKegiatan;
use App\Kegiatan;
use App\Campaign;
use App\Laporan;
use App\Berita;
use App\KonfirmasiTransfer;
use App\Pengurus;
use Illuminate\Support\Facades\Auth;
use Image;
use DB;
use Log;

class UmumController extends Controller
{
				// 	public function get_ip_detail($ip){
				// 		$ip_response = file_get_contents('http://ip-api.com/json/'.$ip);
				// 		$ip_array=json_decode($ip_response);
				// 		return  $ip_array;
				// }
    public function index(){
					$user_ip=$_SERVER['REMOTE_ADDR'];
					if ($user_ip == '127.0.0.1') {
						$ip = "118.136.13.223";
					}else {
						$ip = $user_ip;
					}
					$data = [];
					$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
					if($query && $query['status'] == 'success')
					{
						$date = date('Y-m-d');
						$client = new \GuzzleHttp\Client();
						// $response = $client->request('GET', config('jadwal_sholat')['url'].$query['city'].'/daily.json?key='.config('jadwal_sholat')['key'].'&jsoncallback=?');
// return json_encode($response);
						$response = $client->request('GET', 'https://api.pray.zone/v2/times/day.json?city='.$query['city'].'&date='.$date);
									// echo 'Your City is ' . $query['city'];
									// echo '<br />';
									// echo 'Your State is ' . $query['region'];
									// echo '<br />';
									// echo 'Your Zipcode is ' . $query['zip'];
									// echo '<br />';
									// echo 'Your Coordinates are ' . $query['lat'] . ', ' . $query['lon'];
									// echo json_encode($response->getBody());
									$datas = json_decode($response->getBody(),true);
									$data = $datas['results'];
									$beritas = Berita::where('aktif',1)->orderBy('id','DESC')->paginate(2);
									// return $datas;
					}
					return view('welcome',compact('data','beritas'));
				}
				public function profil(){
					return view('profil');
				}
				public function sejarah(){
					return view('sejarah');
				}
				public function pengurus(){
					$penguruses = Pengurus::where('aktif',1)->get();
					return view('pengurus',compact('penguruses'));
				}
				public function kontak(){
					return view('kontak');
				}
				public function berita(){
					$beritas = Berita::where('aktif',1)->orderBy('id','DESC')->paginate(9);
					return view('berita',compact('beritas'));
				}
				public function detailberita($judul){
					$beritas = Berita::where('judul',$judul)->first();
					$berita = Berita::where('aktif',1)->orderBy('id','DESC')->paginate(10);
					return view('detail_berita',compact('beritas','berita'));
				}
				public function donasi(){
					$dt = Campaign::where('open','=',1)->get();
					$datas = [];
					foreach ($dt as $key => $value) {
						$datas[] = [
							'id'=>$value->id,
							'judul'=>$value->judul,
							'isi'=>$value->isi,
							'gambar'=>$value->gambar,
							'target'=>$value->target,
							'terkumpul'=>Laporan::where('campaign_id',$value->id)->sum('jumlah_paket')
						];
					}
					return view('donasi',compact('datas'));
				}
				public function konfirmasi($id){
					$data = Campaign::where('open','=',1)->where('id',$id)->first();
					$laporans = Laporan::where('campaign_id',$id)->get();
					$terkumpul = Laporan::where('campaign_id',$id)->sum('jumlah_paket');
					return view('konfirmasi_transfer',compact('data','laporans','terkumpul'));
				}
				public function kirimkonfirmasi(Request $request){
					$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
					$validator = validator()->make(request()->all(), $rules);
					if ($validator->fails()) {
						return response()->json([
							'code'=>400,
							'title'=>'Oops...',
							'icon'=>'error',
							'message'=>'Code Captcha Salah',
						]);
					}else {
						DB::beginTransaction();
						try {
							$foto = "bukti-".time().rand(100,999).".jpg";
							$add = KonfirmasiTransfer::create([
								'campaign_id'=>$request->campaign,
								'nama'=>$request->nama,
								'nohp'=>$request->nohp,
								'bukti_transfer'=>$foto,
								'jml_pkt'=>$request->jumlah_paket,
								'pesan'=>$request->pesan,
								'status_transfer_id'=>1
							]);
							$pathfoto = public_path().'/assets/images/bukti/' . $foto;
							Image::make(file_get_contents($request->bukti_transfer))->save($pathfoto);
						} catch (\Throwable $th) {
							Log::info('Gagal konfirmasi:'.$th->getMessage());
							DB::rollback();
							return response()->json([
								'code'=>400,
								'title'=>'Gagal',
								'icon'=>'error',
								'message'=>'Gagal terkirim, Silahkan di ulangi kembali'
							]);
						}
						DB::commit();
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'judul'=>$add->campaignId->judul,
								'foto'=>url('/assets/images/bukti/'.$foto),
								'pesan'=>strtoupper($request->nama).'/'.$request->jumlah_paket.' PAKET.',
								'message'=>'Terimakasi Bapak/Ibu '.$request->nama.' atas donasinya. Semoga Allah SWT membalas kebaikan anda berlipat ganda. Amin...'
							]);
						
					}
				}
				public function sekarang($id){
					$data = Campaign::where('open','=',1)->where('id',$id)->first();
					$laporans = Laporan::where('campaign_id',$id)->get();
					$terkumpul = Laporan::where('campaign_id',$id)->sum('jumlah_paket');
					return view('detail_donasi',compact('data','laporans','terkumpul'));
				}
				public function kegiatan($id){
					$data = KategoriKegiatan::where('name',$id)->first();
					$kegiatans = Kegiatan::where('kategori_kegiatan_id',$data->id)->where('aktif','=',1)->get();
					return view('kegiatan',compact('data','kegiatans'));
				}
				public function laporan_wakaf(){
					$dt = Campaign::where('open','=',1)->get();
					$datas = [];
					foreach ($dt as $key => $value) {
						$datas[] = [
							'id'=>$value->id,
							'judul'=>$value->judul,
							'isi'=>$value->isi,
							'gambar'=>$value->gambar,
							'target'=>$value->target,
							'terkumpul'=>Laporan::where('campaign_id',$value->id)->sum('jumlah_paket')
						];
					}
					// return $dt;
					return view('laporan.wakaf',compact('datas'));
				}
				public function detail_laporan_wakaf($id){
					$data = Campaign::where('open','=',1)->where('id',$id)->first();
					$laporans = Laporan::where('campaign_id',$id)->where('aktif',1)->get();
					$terkumpul = Laporan::where('campaign_id',$id)->where('aktif',1)->sum('jumlah_paket');
					return view('laporan.detail_laporan_wakaf',compact('laporans','data','terkumpul'));
				}
				public function laporan_infaq(){
					return view('laporan.infaq');
				}
				public $successStatus = 200;
				public function masuk(Request $request){ 
					$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
					$validator = validator()->make(request()->all(), $rules);
					if ($validator->fails()) {
						return response()->json([
							'code'=>400,
							'title'=>'Oops...',
							'icon'=>'error',
							'message'=>'Code Captcha Salah',
						]);
					}else {
						if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
										$respon = [
														'code'=>200,
														'title'=>'Berhasil',
														'icon'=>'success',
														'message'=>'Login berhasil'
										];
										return response()->json($respon, 200); 
						}else{ 
										return response()->json([
														'code'=>'400',
														'title'=>'Gagal',
														'icon'=>'error',
														'message'=>'Email atau Kata Sandi Salah',
										]); 
						} 
					}		
				}
				public function campaign(){
					$data = Campaign::where('open','=',1)->get();
					return response()->json([
						'code'=>200,
						'data'=>$data
					]);
				}
}
