<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\TipeUser;
use App\KonfirmasiTransfer;
use App\Laporan;
use DB;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user.welcome');
				}
				public function datauser(Request $request){
					$users = User::get();
					return view('user.data_user',compact('users'));
				}
				public function getTipe(){
						$data = TipeUser::get();
						return response()->json([
							'code'=>200,
							'message'=>'TipeUser',
							'data'=>$data
						]);
				}
				public function pengajuan_transfer(Request $request){
					if ($request->ids && $request->action == 'proses') {
						$cek = KonfirmasiTransfer::where('id',$request->ids)->where('status_transfer_id',1)->first();
						if ($cek) {
							DB::beginTransaction();
							try {
								$proses = KonfirmasiTransfer::where('id',$request->ids)->where('status_transfer_id',1)->first();
								$proses->status_transfer_id = 2;
								$proses->update();
								$laporan = Laporan::create([
									'campaign_id'=>$proses->campaign_id,
									'nama'=>$proses->nama,
									'jumlah_paket'=>$proses->jml_pkt,
									'admin'=>$request->user()->id
								]);
							} catch (\Throwable $th) {
								DB::rollback();
								return response()->json([
									'code'=>400,
									'title'=>'Gagal',
									'icon'=>'error',
									'message'=>'Gagal Diproses'
								]);
							}
							DB::commit();
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'message'=>'Berhasil Diproses'
							]);
							

						}
						return response()->json([
							'code'=>400,
							'title'=>'Gagal',
							'icon'=>'error',
							'message'=>'Data Tidak Ditemukan'
						]);
					}elseif ($request->ids && $request->action == 'batal') {
						$cek = KonfirmasiTransfer::where('id',$request->ids)->where('status_transfer_id',1)->first();
						if ($cek) {
							DB::beginTransaction();
							try {
								$proses = KonfirmasiTransfer::where('id',$request->ids)->where('status_transfer_id',1)->first();
								$proses->status_transfer_id = 3;
								$proses->update();
							} catch (\Throwable $th) {
								DB::rollback();
								return response()->json([
									'code'=>400,
									'title'=>'Gagal',
									'icon'=>'error',
									'message'=>'Gagal dibatalkan'
								]);
							}
							DB::commit();
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'message'=>'Berhasil dibatalkan'
							]);
							

						}
						return response()->json([
							'code'=>400,
							'title'=>'Gagal',
							'icon'=>'error',
							'message'=>'Data Tidak Ditemukan'
						]);
					}
					return view('user.konfirmasi_donasi');
				}
				public function getTransfer(Request $request){
					$data = KonfirmasiTransfer::get();
					$dt = [];
					foreach ($data as $key => $value) {
						$dt[] = [
							'id'=>$value->id,
							'campaign_id'=>$value->campaignId->judul,
							'tanggal'=>date('d M Y H:i', strtotime($value->created_at)),
							'nama'=>$value->nama,
							'nohp'=>$value->nohp,
							'jml_pkt'=>$value->jml_pkt,
							'pesan'=>$value->pesan,
							'bukti_transfer'=>url('assets/images/bukti').'/'.$value->bukti_transfer,
							'status'=>$value->statusTransfer->status,
							'status_transfer_id'=>$value->status_transfer_id
						];
					}
					return response()->json([
						'code'=>200,
						'data'=>$dt
					]);
				}
				public function proses_pengajuan_transfer($id){
					$datas = KonfirmasiTransfer::where('id',$id)->where('status_transfer_id','=',1)->first();
					if ($datas) {
						$datas->status_transfer_id = 2;
						$datas->update();
					}

				}
				public function input_laporan(Request $request){
					if ($request->captcha && $request->action =='add') {
						$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
						$validator = validator()->make(request()->all(), $rules);
								if ($validator->fails()) {
									return response()->json([
										'code'=>400,
										'title'=>'Oops...',
										'icon'=>'error',
										'message'=>'Code Captcha Salah',
									]);
								}else {
									$add = Laporan::create([
										'campaign_id'=>$request->campaign_id,
										'nama'=>$request->nama,
										'jumlah_paket'=>$request->jumlah_paket,
										'admin'=>$request->user()->id,
										'aktif'=>1
									]);
									if ($add) {
										return response()->json([
											'code'=>200,
											'title'=>'Berhasil',
											'icon'=>'success',
											'message'=>'Input Laporan Berhasil'
										]);
									}
									return response()->json([
										'code'=>400,
										'title'=>'Gagal',
										'icon'=>'error',
										'message'=>'Input Laporan Gagal'
									]);
								}
					}elseif ($request->captcha && $request->action =='edit') {
						$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
						$validator = validator()->make(request()->all(), $rules);
								if ($validator->fails()) {
									return response()->json([
										'code'=>400,
										'title'=>'Oops...',
										'icon'=>'error',
										'message'=>'Code Captcha Salah',
									]);
								}else {
									$add = Laporan::find($request->ids)->update([
										'campaign_id'=>$request->campaign_id,
										'nama'=>$request->nama,
										'jumlah_paket'=>$request->jumlah_paket,
										'admin'=>$request->user()->id
									]);
									if ($add) {
										return response()->json([
											'code'=>200,
											'title'=>'Berhasil',
											'icon'=>'success',
											'message'=>'Update Laporan Berhasil'
										]);
									}
									return response()->json([
										'code'=>400,
										'title'=>'Gagal',
										'icon'=>'error',
										'message'=>'Update Laporan Gagal'
									]);
								}
					}elseif ($request->action =='hapus') {
						$add = Laporan::find($request->ids)->update([
							'aktif'=>0,
							'admin'=>$request->user()->id
						]);
						if ($add) {
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'message'=>'Hapus Laporan Berhasil'
							]);
						}
						return response()->json([
							'code'=>400,
							'title'=>'Gagal',
							'icon'=>'error',
							'message'=>'Hapus Laporan Gagal'
						]);
					}
					return view('user.input_laporan');
				}
}
