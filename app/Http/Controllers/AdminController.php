<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\TipeUser;
use Illuminate\Support\Facades\Hash;
use App\Campaign;
use App\Laporan;
use App\Berita;
use App\Kegiatan;
use App\Pengurus;
use Image;
use DB;
use Log;

class AdminController extends Controller
{
	public function __construct()
	{
					$this->middleware('admin');
	}

	public function datauser(Request $request){
		$users = User::get();
		return view('user.data_user',compact('users'));
	}
	public function adduser(Request $request){
		if ($request->action == 'add') {
			if ($request->nama && $request->email && $request->password && $request->tipe) {
				DB::beginTransaction();
				try {
					$add = User::create([
						'name'=>$request->nama,
						'email'=>$request->email,
						'password'=>Hash::make($request->password),
						'tipe_user_id'=>$request->tipe
					]);
				} catch (\Throwable $th) {
					Log::info('Gagal Add user:'.$th->getMessage());
					DB::rollback();
					return response()->json([
						'code'=>400,
						'title'=>'Gagal',
						'icon'=>'error',
						'message'=>'Gagal ditambah'
					]);
				}
				DB::commit();
				return response()->json([
					'code'=>200,
					'title'=>'Berhasil',
					'icon'=>'success',
					'message'=>'Berhasil ditambah'
				]);
				
			}
		}elseif($request->action == 'edit'){
			if ($request->nama && $request->email && $request->password && $request->tipe) {
				DB::beginTransaction();
				try {
					$add = User::where('id',$request->ids)->where('aktif',1)->update([
						'name'=>$request->nama,
						'email'=>$request->email,
						'password'=>Hash::make($request->password),
						'tipe_user_id'=>$request->tipe
					]);
				} catch (\Throwable $th) {
					Log::info('Gagal diedit user:'.$th->getMessage());
					DB::rollback();
					return response()->json([
						'code'=>400,
						'title'=>'Gagal',
						'icon'=>'error',
						'message'=>'Gagal diedit'
					]);
				}
				DB::commit();
				return response()->json([
					'code'=>200,
					'title'=>'Berhasil',
					'icon'=>'success',
					'message'=>'Berhasil diedit'
				]);
				
			}
		}
		
		$tipes = TipeUser::get();
		return view('user.add_user',compact('tipes'));
	}
	public function edituser($id){
		$user = User::where('id',$id)->where('aktif',1)->first();
		return view('user.edit_user',compact('user'));
	}
	public function nonaktifuser($id){
		$user = User::where('id',$id)->where('aktif',1)->first();
		$user->aktif = 0;
		$user->update();
		return redirect()->route('data-user');
	}
	public function aktifuser($id){
		$user = User::where('id',$id)->where('aktif',0)->first();
		$user->aktif = 1;
		$user->update();
		return redirect()->route('data-user');
	}
	public function galang_dana(Request $request){
		if ($request->captcha) {
						$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
						$validator = validator()->make(request()->all(), $rules);
						if ($validator->fails()) {
							return response()->json([
								'code'=>400,
								'title'=>'Oops...',
								'icon'=>'error',
								'message'=>'Code Captcha Salah',
							]);
						}else {
							DB::beginTransaction();
							try {
								$gambar = "Campaign-".time().rand(100,999).".jpg";
								$add = Campaign::create([
									'gambar'=>$gambar,
									'judul'=>$request->judul,
									'isi'=>$request->pesan,
									'target'=>$request->target,
									'admin'=>$request->user()->id,
									'open'=>1
								]);
								$pathfoto = public_path().'/assets/images/campaign/' . $gambar;
								Image::make(file_get_contents($request->gambar))->resize(360, 202)->save($pathfoto);
							} catch (\Throwable $th) {
								//throw $th;
								Log::info('Gagal galang:'.$th->getMessage());
								DB::rollback();
								return response()->json([
									'code'=>400,
									'title'=>'Gagal',
									'icon'=>'error',
									'message'=>'Gagal simpan data.'
								]);
							}
							DB::commit();
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'message'=>'Berhasil simpan data.'
							]);
						}
		}
		return view('user.galang_dana');
	}
	public function berita_baru(Request $request){
		if ($request->action == 'add') {
			$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
			$validator = validator()->make(request()->all(), $rules);
					if ($validator->fails()) {
						return response()->json([
							'code'=>400,
							'title'=>'Oops...',
							'icon'=>'error',
							'message'=>'Code Captcha Salah',
						]);
					}else {
						DB::beginTransaction();
						try {
								$foto = "berita-".time().rand(100,999).".jpg";
								$add = Berita::create([
									'gambar'=>$foto,
									'judul'=>$request->judul,
									'isi'=>$request->isi,
									'admin_id'=>$request->user()->id,
									'aktif'=>1
								]);
								$pathfoto = public_path().'/assets/images/berita/' . $foto;
								Image::make(file_get_contents($request->gambar))->resize(860, 600)->save($pathfoto);
						} catch (\Throwable $th) {
							//throw $th;
								Log::info('Gagal Add  berita:'.$th->getMessage());
								DB::rollback();
								return response()->json([
									'code'=>400,
									'title'=>'Gagal',
									'icon'=>'error',
									'message'=>'Gagal simpan data.'
								]);
						}
							DB::commit();
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'message'=>'Berhasil simpan data.'
							]);
						
					}
		}
		return view('user.add_berita');
	}
	public function input_laporan(Request $request){
		if ($request->captcha && $request->action =='add') {
			$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
			$validator = validator()->make(request()->all(), $rules);
					if ($validator->fails()) {
						return response()->json([
							'code'=>400,
							'title'=>'Oops...',
							'icon'=>'error',
							'message'=>'Code Captcha Salah',
						]);
					}else {
						$add = Laporan::create([
							'campaign_id'=>$request->campaign_id,
							'nama'=>$request->nama,
							'jumlah_paket'=>$request->jumlah_paket,
							'admin'=>$request->user()->id,
							'aktif'=>1
						]);
						if ($add) {
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'message'=>'Input Laporan Berhasil'
							]);
						}
						return response()->json([
							'code'=>400,
							'title'=>'Gagal',
							'icon'=>'error',
							'message'=>'Input Laporan Gagal'
						]);
					}
		}elseif ($request->captcha && $request->action =='edit') {
			$rules = ['captcha' => 'required|captcha_api:'. request('key') . ',flat'];
			$validator = validator()->make(request()->all(), $rules);
					if ($validator->fails()) {
						return response()->json([
							'code'=>400,
							'title'=>'Oops...',
							'icon'=>'error',
							'message'=>'Code Captcha Salah',
						]);
					}else {
						$add = Laporan::find($request->ids)->update([
							'campaign_id'=>$request->campaign_id,
							'nama'=>$request->nama,
							'jumlah_paket'=>$request->jumlah_paket,
							'admin'=>$request->user()->id
						]);
						if ($add) {
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'icon'=>'success',
								'message'=>'Update Laporan Berhasil'
							]);
						}
						return response()->json([
							'code'=>400,
							'title'=>'Gagal',
							'icon'=>'error',
							'message'=>'Update Laporan Gagal'
						]);
					}
		}elseif ($request->action =='hapus') {
			$add = Laporan::find($request->ids)->update([
				'aktif'=>0,
				'admin'=>$request->user()->id
			]);
			if ($add) {
				return response()->json([
					'code'=>200,
					'title'=>'Berhasil',
					'icon'=>'success',
					'message'=>'Hapus Laporan Berhasil'
				]);
			}
			return response()->json([
				'code'=>400,
				'title'=>'Gagal',
				'icon'=>'error',
				'message'=>'Hapus Laporan Gagal'
			]);
		}
		return view('user.input_laporan');
	}
	public function edit_wakaf($id){
		$laporan = Laporan::find($id);
		return view('user.edit_wakaf',compact('laporan'));
	}
	public function upload_kegiatan(Request $request){
		if ($request->foto) {
			DB::beginTransaction();
			try {
				$foto = "kegiatan-".time().rand(100,999).".jpg";
				$add = Kegiatan::create([
					'kategori_kegiatan_id'=>$request->id,
					'foto'=>$foto,
					'judul'=>'Kegiatan '.$request->name,
					'admin'=>$request->user()->id,
					'aktif'=>1
				]);
				$pathfoto = public_path().'/assets/images/kegiatan/' . $foto;
				Image::make(file_get_contents($request->foto))->resize(1280, 959)->save($pathfoto);
			} catch (\Throwable $th) {
				Log::info('Gagal upload kegiatan:'.$th->getMessage());
				DB::rollback();
				return response()->json([
					'code'=>400,
					'title'=>'Gagal',
					'icon'=>'error',
					'message'=>'Gagal upload foto.'
				]);
		}
			DB::commit();
			return response()->json([
				'code'=>200,
				'title'=>'Berhasil',
				'icon'=>'success',
				'message'=>'Berhasil upload foto.'
			]);
		}
	}
	public function add_pengurus(Request $request){
		if ($request->action == 'add') {
			DB::beginTransaction();
			try {
				$foto = "pengurus-".time().rand(100,999).".jpg";
				$add = Pengurus::create([
					'nama'=>$request->nama,
					'foto'=>$foto,
					'admin_id'=>$request->user()->id,
					'aktif'=>1
					]);
				$pathfoto = public_path().'/assets/images/pengurus/' . $foto;
				Image::make(file_get_contents($request->foto))->resize(111, 111)->save($pathfoto);
			} catch (\Throwable $th) {
				Log::info('Gagal upload kegiatan:'.$th->getMessage());
				DB::rollback();
				return response()->json([
					'code'=>400,
					'title'=>'Gagal',
					'icon'=>'error',
					'message'=>'Gagal simpan data.'
				]);
		}
			DB::commit();
			return response()->json([
				'code'=>200,
				'title'=>'Berhasil',
				'icon'=>'success',
				'message'=>'Berhasil simpan data.'
			]);
			
		}
		return view('user.add_pengurus');
	}
}
