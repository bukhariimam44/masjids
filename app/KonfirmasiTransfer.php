<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KonfirmasiTransfer extends Model
{
	protected $fillable = [
		'campaign_id','nama', 'nohp', 'jml_pkt','pesan','bukti_transfer','status_transfer_id'
];
public function statusTransfer(){
	return $this->belongsTo('App\StatusTransfer','status_transfer_id');
}
public function campaignId(){
	return $this->belongsTo('App\Campaign','campaign_id');
}
}
