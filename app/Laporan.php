<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
	protected $fillable = [
		'campaign_id','nama', 'jumlah_paket', 'admin','aktif'
];
public function oneCamp(){
	return $this->hasOne('App\Campaign','campaign_id');
}
}
