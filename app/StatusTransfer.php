<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusTransfer extends Model
{
	protected $fillable = [
		'status'
];
}
