<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
	protected $fillable = [
		'judul', 'isi','gambar', 'admin_id','aktif'
];
public function userId(){
	return $this->belongsTo('App\User','admin_id');
}
}
