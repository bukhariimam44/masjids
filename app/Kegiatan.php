<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
	protected $fillable = [
		'kategori_kegiatan_id','judul', 'foto', 'admin','aktif'
];
}
