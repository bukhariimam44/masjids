<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriKegiatan extends Model
{
	protected $fillable = [
		'name','bg','tentang'
];
}
