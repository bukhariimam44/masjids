<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengurus extends Model
{
	protected $fillable = [
		'nama', 'foto', 'admin_id','aktif'
];
}
