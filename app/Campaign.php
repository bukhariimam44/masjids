<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
	protected $fillable = [
		'gambar','judul', 'isi', 'target','open','admin'
];
}
