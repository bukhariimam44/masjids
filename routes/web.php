<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UmumController@index')->name('index');
Route::get('/profil', 'UmumController@profil')->name('profil');
Route::get('/sejarah', 'UmumController@sejarah')->name('sejarah');
Route::get('/pengurus', 'UmumController@pengurus')->name('pengurus');
Route::get('/kontak', 'UmumController@kontak')->name('kontak');
Route::get('/berita', 'UmumController@berita')->name('berita');
Route::get('/berita/{judul}', 'UmumController@detailberita')->name('detail-berita');
Route::get('/donasi', 'UmumController@donasi')->name('donasi');
Route::get('/donasi-sekarang/{id}', 'UmumController@sekarang')->name('donasi-sekarang');
Route::get('/konfirmasi-transfer/{id}', 'UmumController@konfirmasi')->name('konfirmasi-transfer');
Route::get('/kegiatan/{id}', 'UmumController@kegiatan')->name('kegiatan');
Route::get('/laporan-wakaf', 'UmumController@laporan_wakaf')->name('laporan-wakaf');
Route::get('/detail-laporan-wakaf-{id}', 'UmumController@detail_laporan_wakaf')->name('detail-laporan-wakaf');
Route::get('/laporan-infaq', 'UmumController@laporan_infaq')->name('laporan-infaq');

Route::post('/masuk', 'UmumController@masuk')->name('masuk');

Auth::routes();
Route::get('/getTipe', 'HomeController@getTipe')->name('getTipe');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/pengajuan-transfer', 'HomeController@pengajuan_transfer')->name('pengajuan-transfer');
Route::post('/pengajuan-transfer', 'HomeController@pengajuan_transfer')->name('pengajuan-transfer');
Route::get('/getTransfer', 'HomeController@getTransfer')->name('getTransfer');
Route::get('/proses-pengajuan-transfer/{id}', 'HomeController@proses_pengajuan_transfer')->name('proses-pengajuan-transfer');

Route::get('/keluar', 'HomeController@keluar')->name('keluar');
//ADMIN
Route::get('/data-user', 'AdminController@datauser')->name('data-user');
Route::get('/add-user', 'AdminController@adduser')->name('add-user');
Route::post('/add-user', 'AdminController@adduser')->name('add-user');
Route::get('/edit-user{id}', 'AdminController@edituser')->name('edit-user');
Route::get('/nonaktif-user{id}', 'AdminController@nonaktifuser')->name('nonaktif-user');
Route::get('/aktif-user{id}', 'AdminController@aktifuser')->name('aktif-user');
Route::get('/galang-dana', 'AdminController@galang_dana')->name('galang-dana');
Route::post('/galang-dana', 'AdminController@galang_dana')->name('galang-dana');
Route::get('/berita-baru', 'AdminController@berita_baru')->name('berita-baru');
Route::post('/berita-baru', 'AdminController@berita_baru')->name('berita-baru');
Route::get('/input-laporan', 'HomeController@input_laporan')->name('input-laporan');
Route::get('/edit-wakaf{id}', 'AdminController@edit_wakaf')->name('edit-wakaf');
Route::post('/input-laporan', 'HomeController@input_laporan')->name('input-laporan');
Route::post('/upload-kegiatan', 'AdminController@upload_kegiatan')->name('upload-kegiatan');
Route::get('/add-pengurus', 'AdminController@add_pengurus')->name('add-pengurus');
Route::post('/add-pengurus', 'AdminController@add_pengurus')->name('add-pengurus');
