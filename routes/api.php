<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/kirim-transfer', 'UmumController@kirimkonfirmasi')->name('kirim-transfer');
Route::get('/get-captcha/{id}','\Mews\Captcha\CaptchaController@getCaptchaApi');
Route::get('/get-campaign','UmumController@campaign')->name('get-campaign');
