@extends('layouts.app')

@section('content')
        <section>
            <div class="gap no-gap">
                <img class="botm-shp shp-img" src="assets/images/shp1.png" alt="shp1.png">
                <div class="featured-area-wrap text-center">
                    <div class="featured-area owl-carousel">
                        <div class="featured-item" style="background-image: url(assets/images/resources/slider1.png);">
                            <div class="featured-cap">
                                <img src="assets/images/resources/bsml-txt.png" alt="bsml-txt.png">
                                <!-- <h1><img src="assets/images/resources/ayat-txt.png" alt="ayat-txt.png"></h1> -->
                                <h3><br>Wakaf Pembebasan Lahan & Pembangunan</h3>
                                <span>Asrama Tahfidz Qur'an</span>
                                <a class="thm-btn brd-rd40" href="{{route('donasi')}}" title="">DONASI</a>
                            </div>
                        </div>
                        <div class="featured-item" style="background-image: url(assets/images/resources/slider2.png);">
                            <div class="featured-cap">
                                <img src="assets/images/resources/bsml-txt2.png" alt="bsml-txt2.png">
                                <!-- <h1><img src="assets/images/resources/ayat-txt2.png" alt="ayat-txt2.png"></h1> -->
                                <h3><br>Kami bagi 6,8 Miliyar menjadi 68.000 Paket</h3>
                                <span>@ Rp 100.000 / Paket</span>
                                <a class="thm-btn brd-rd40" href="{{route('donasi')}}" title="">DONASI</a>
                            </div>
                        </div>
                    </div>
                </div><!-- Featured Area Wrap -->
            </div>
        </section>
        <section>
            <div class="gap">
                <div class="lft-shp shp-lyr"></div>
                <div class="container">
                    <div class="plrs-wrap text-center remove-ext5">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="plr-box">
                                    <i class="flaticon-charity"></i>
                                    <div class="pilr-info">
                                        <h4>Sedekah</h4>
                                        <p>Sedekah adalah mengamalkan atau menginfakan harta di jalan Allah. Namun, kegiatan ini bukan hanya semata-mata menginfakan harta di jalan Allah atau menyisihkan sebagian uang pada fakir miskin, tetapi shadaqah juga mencakup segala macam dzikir (tasbih, tahmid, dan tahlil) dan segala macam perbuatan baik lainnya.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="plr-box">
                                    <i class="flaticon-mosque"></i>
                                    <div class="pilr-info">
                                        <h4>Wakaf</h4>
                                        <p>Wakaf adalah menahan harta yang dapat dimanfaatkan untuk kepentingan umum tanpa mengurangi nilai harta. pahala wakaf jauh lebih besar lantaran manfaatnya dirasakan oleh banyak orang dan sifatnya kekal. Pahala wakaf akan terus mengalir meskipun wakifnya telah meninggal dunia.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="plr-box">
                                    <i class="flaticon-charity"></i>
                                    <div class="pilr-info">
                                        <h4>Infaq</h4>
                                        <p>Infaq adalah pengeluaran sukarela yang di lakukan seseorang, setiap kali ia memperoleh rizki, sebanyak yang ia kehendakinya. Allah memberi kebebasan kepada pemiliknya untuk menentukan jenis harta, berapa jumlah yang yang sebaiknya diserahkan.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="plr-box">
                                    <i class="flaticon-kaaba-building"></i>
                                    <div class="pilr-info">
                                        <h4>Haji</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div><!-- Pillers Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap top-spac120 white-grad-layer">
                <img class="top-shp shp-img" src="assets/images/shp2.png" alt="shp2.png">
                <img class="botm-shp shp-img" src="assets/images/shp3.png" alt="shp3.png">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax1.jpg);"></div>
                <div class="container">
                    <div class="abot-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-lg-6">
                                <div class="abot-sec">
                                    <div class="sec-title">
                                        <div class="sec-title-inner">
                                            <span>Tentang</span>
                                            <h3>AL-AZZAM</h3>
                                        </div>
                                    </div>
                                    <p>Sebuah Gerakan dari Aksi Cepat Tanggap untuk Menghadirkan Kedermawanan Melalui Sedekah, Wakaf dan Infaq. Mari Raih Keberkahan!</p>
                                    <a class="secndry-btn brd-rd40" href="{{route('donasi')}}" title="">Mari Donasi</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- About Wrap -->
                </div>
            </div>
        </section>
        {{--<section>
            <div class="gap top-spac60">
                <div class="fixed-bg bg-mode-clr" style="background-image: url(assets/images/parallax2.png);"></div>
                <div class="container">
                    <div class="sec-title text-center">
                        <div class="sec-title-inner">
                            <span>Serving Humanity</span>
                            <h3>Our Services</h3>
                        </div>
                    </div>
                    <div class="serv-wrap">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-lg-4">
                                <div class="srv-tl">
                                    <h2>We're on a <span>Mission</span> to</h2>
                                    <h5>solve the problems and reinvent charity for a new genration.</h5>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 col-lg-8">
                                <div class="remove-ext9">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="serv-box">
                                                <i class="flaticon-quran-rehal"></i>
                                                <div class="serv-info">
                                                    <h4>Quran Learning</h4>
                                                    <p>Lorem ipsum dolor sit amet, coteudtur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="serv-box">
                                                <i class="flaticon-grave"></i>
                                                <div class="serv-info">
                                                    <h4>Funeral Service</h4>
                                                    <p>Lorem ipsum dolor sit amet, coteudtur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="serv-box">
                                                <i class="flaticon-mosque"></i>
                                                <div class="serv-info">
                                                    <h4>Mosque Development</h4>
                                                    <p>Lorem ipsum dolor sit amet, coteudtur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="serv-box">
                                                <i class="flaticon-social-care"></i>
                                                <div class="serv-info">
                                                    <h4>Help Poor's</h4>
                                                    <p>Lorem ipsum dolor sit amet, coteudtur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Serv Wrap -->
                </div>
            </div>
        </section>--}}
        <section id="waktu_sholat">
            <div class="gap top-spac120 bottom-spac140 gray-layer opc95">
                <img class="botm-shp shp-img" src="assets/images/shp5.png" alt="shp5.png">
                <img class="top-shp shp-img" src="assets/images/shp6.png" alt="shp6.png">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax4.jpg);"></div>
                <div class="container">
                    <div class="evnt-pry-wrap">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 col-lg-8">
                                <div class="sec-title">
                                    <div class="sec-title-inner">
                                        <h3><span>Berita</span> Terbaru</h3>
                                    </div>
                                    <p>There are many variations of passages of Lorem Ipsum available.</p>
                                </div>
                                <div class="evnt-wrap remove-ext5">
                                    <div class="row mrg20">
																																						@foreach($beritas as $berita)
																																						<?php $jumlah_karakter = strlen($berita->judul);?>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="evnt-box">
                                                <div class="evnt-thmb">
                                                    <a href="{{route('detail-berita',$berita->judul)}}" title=""><img src="{{asset('assets/images/berita/'.$berita->gambar)}}" alt="{{$berita->judul}}"></a>
                                                </div>
                                                <div class="evnt-info">
                                                    <h4><a href="{{route('detail-berita',$berita->judul)}}" title="">{{substr($berita->judul, 0, 25)}} @if($jumlah_karakter > 25) ... @endif</a></h4>
                                                    <ul class="pst-mta">
                                                        <li class="thm-clr">{{$berita->userId->name}}</li>
                                                        <li>{{date('d M Y. H:i',strtotime($berita->created_at))}}</li>
                                                    </ul>
                                                    <p>{!! substr($berita->isi, 0, 106) !!}</p>
                                                </div>
                                            </div>
                                        </div>
																																						@endforeach
                                    </div>
                                </div><!-- Events Wrap -->
                            </div>
                            <div class="col-md-4 col-sm-12 col-lg-4">
                                <div class="sec-title">
                                    <div class="sec-title-inner">
                                        <h3>Waktu <span>Sholat</span></h3>
                                    </div>
                                </div>
                                <ul class="prayer-times">
                                    <li class="pry-tim-hed"><span>Sholat</span><span>Jam Mulai</span>{{--<span>Iqamah</span>--}}</li>
                                    <li><span class="thm-clr">Imsak</span><span>{{$data['datetime'][0]['times']['Imsak']}}</span>{{--<span>04:45 am</span>--}}</li>
                                    <li><span class="thm-clr">Subuh</span><span>{{$data['datetime'][0]['times']['Fajr']}}</span>{{--<span>05:31 am</span>--}}</li>
                                    <li><span class="thm-clr">Dzuhur</span><span>{{$data['datetime'][0]['times']['Dhuhr']}}</span>{{--<span>12:47 pm</span>--}}</li>
                                    <li><span class="thm-clr">Ashar</span><span>{{$data['datetime'][0]['times']['Asr']}}</span>{{--<span>05:50 pm</span>--}}</li>
                                    <li><span class="thm-clr">Maghrib</span><span>{{$data['datetime'][0]['times']['Maghrib']}}</span>{{--<span>08:04 pm</span>--}}</li>
                                    <li><span class="thm-clr">Isha</span><span>{{$data['datetime'][0]['times']['Isha']}}</span>{{--<span>09:30 pm</span>--}}</li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- Events & Prayer Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap thm-layer opc9">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax7.jpg);"></div>
                <div class="container">
                    <div class="nwsltr-wrp text-center">
                        <div class="nwsltr-innr">
                            <div class="nwsltr-title">
                                <h3>Berlangganan Berita</h3>
                                <span>Berlangganan untuk menerima informasi berita melalui Email kamu.</span>
                            </div>
                            <form>
                                <input type="email" placeholder="Masukan Email kamu">
                                <button type="submit">Berlangganan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
								</section>
								@endsection
								@section('js')
								<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
								<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
								<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
								<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
								<script>
    var waktu_sholat = new Vue({
        el:'#waktu_sholat',
        data:{
									waktu:''
								}
								});
								</script>
								@endsection