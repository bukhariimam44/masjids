@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax13.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="assets/images/resources/page-title-ayat.png" alt="page-title-ayat.png"></h1> -->
                        <h2>Berita</h2>
                        <ul class="breadcrumbs">
                            <li><a href="{{route('index')}}" title="">Beranda</a></li>
                            <li>Berita</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap">
                <div class="container">
																@if(Auth::check())
																		@if(Auth::user()->tipe_user_id == 1)
																			<a href="{{route('berita-baru')}}" class="btn btn-success">Berita Baru</a>
																			<br><br>
																		@endif
																@endif
                    <div class="blog-wrap remove-ext7">
                        <div class="row mrg40">
																												@if(count($beritas) < 1)
																												<div class="blog-box">
																												<div class="blog-thmb">
																												<center><h4>Berita Kosong</h4></center>
																												</div>
																												</div>
																												@endif
																												@foreach($beritas as $key => $berita)
                            <div class="col-md-4 col-sm-6 col-lg-4 fadeIn" data-wow-duration=".8s" data-wow-delay=".2s">
                                <div class="blog-box">
                                    <div class="blog-thmb">
                                        <a href="{{route('detail-berita',$berita->judul)}}" title=""><img src="{{asset('assets/images/berita/'.$berita->gambar)}}" alt="{{$berita->judul}}"></a>
                                    </div>
                                    <div class="blog-info">
                                        <!-- <ul class="pst-mta2">
                                            <li><a href="#" title="">Islamic, History</a></li>
                                        </ul> -->
																																								<?php $jumlah_karakter = strlen($berita->judul);
																																								?>
                                        <h4><a href="{{route('detail-berita',$berita->judul)}}" title="">@if($jumlah_karakter > 27){{substr($berita->judul, 0, 27)}} ... @else {{substr($berita->judul, 0, 27)}} @endif</a></h4>
                                        <p>{!! substr($berita->isi, 0, 106) !!} ...</p>
                                        <a href="{{route('detail-berita',$berita->judul)}}" title="">Baca Semua</a>
                                    </div>
                                </div>
                            </div>
																												@endforeach
                        </div>
                    </div><!-- Blog Wrap -->
                    <div class="pagination-wrap text-center">
																				{{ $beritas->links() }}
                        <!-- <ul class="pagination">
                            <li><a href="#" title="">1</a></li>
                            <li class="active"><a href="#" title="">2</a></li>
                            <li><a href="#" title="">3</a></li>
                            <li>................</li>
                            <li><a href="#" title="">10</a></li>
                        </ul> -->
                    </div><!-- Pagination Wrap -->
                </div>
            </div>
        </section>
@endsection
@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
				var attrs = { };

				$.each($("span")[0].attributes, function(idx, attr) {
								attrs[attr.nodeName] = attr.nodeValue;
				});


				$("span").replaceWith(function () {
								attrs.text = $(this).text();
								return $("<a />", attrs);
				});

    $("li").removeClass("page-item");
				$("a").removeClass("page-link");
});
</script>
@endsection