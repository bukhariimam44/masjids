@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax15.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="assets/images/resources/page-title-ayat.png" alt="page-title-ayat.png"></h1> -->
                        <h2>Pengurus</h2>
                        <ul class="breadcrumbs">
                            <li><a href="index.html" title="">Beranda</a></li>
                            <li>Pengurus</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap">
                <div class="container">
                    <div class="sec-title text-center">
                        <div class="sec-title-inner">
                            <!-- <span>Our Expert</span> -->
                            <h3>Pengurus AL-AZZAM</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div class="team-wrap2 text-center remove-ext3">
																				@if(Auth::check())
																		@if(Auth::user()->tipe_user_id == 1)
																			<a href="{{route('add-pengurus')}}" class="btn btn-success">Tambah Pengurus</a>
																			<br><br>
																		@endif
																@endif
                        <div class="row">
																												@foreach($penguruses as $key=> $pengurus)
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="team-box2">
                                    <img src="{{asset('assets/images/pengurus/'.$pengurus->foto)}}" alt="{{$pengurus->nama}}">
                                    <div class="team-info2">
                                        <h4>{{$pengurus->nama}}</h4>
                                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p> -->
                                        <!-- <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
																												@endforeach
                            {{--<div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="team-box2">
                                    <img src="assets/images/resources/team-img2-2.jpg" alt="team-img2-2.jpg">
                                    <div class="team-info2">
                                        <h4>Umair</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="team-box2">
                                    <img src="assets/images/resources/team-img2-3.jpg" alt="team-img2-3.jpg">
                                    <div class="team-info2">
                                        <h4>Fatima</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="team-box2">
                                    <img src="assets/images/resources/team-img2-4.jpg" alt="team-img2-4.jpg">
                                    <div class="team-info2">
                                        <h4>Ammara</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="team-box2">
                                    <img src="assets/images/resources/team-img2-5.jpg" alt="team-img2-5.jpg">
                                    <div class="team-info2">
                                        <h4>AL Zahra</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="team-box2">
                                    <img src="assets/images/resources/team-img2-6.jpg" alt="team-img2-6.jpg">
                                    <div class="team-info2">
                                        <h4>Umair</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="team-box2">
                                    <img src="assets/images/resources/team-img2-7.jpg" alt="team-img2-7.jpg">
                                    <div class="team-info2">
                                        <h4>Fatima</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="team-box2">
                                    <img src="assets/images/resources/team-img2-8.jpg" alt="team-img2-8.jpg">
                                    <div class="team-info2">
                                        <h4>Ammara</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                        <div class="team-scl">
                                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                    </div><!-- Team Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap blue-layer opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax9.jpg);"></div>
                <div class="container">
                    <div class="newsletter-wrap2 text-center">
                        <div class="newsletter-inner">
                            <div class="nwsltr-title2 text-center">
                                <span>BERLANGGANAN BERITA TERBARU</span>
                                <h3>Ingin Sesuatu yang Ekstra?</h3>
                            </div>
                            <form>
                                <input type="email" placeholder="Email kamu">
                                <button class="thm-btn" type="submit">Berlangganan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection