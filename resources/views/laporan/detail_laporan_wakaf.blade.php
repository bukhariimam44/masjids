@extends('layouts.app')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
table tbody { display:block; max-height:450px; overflow-y:scroll; }
table thead, table tbody tr { display:table; width:100%; table-layout:fixed; }
</style>
<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet"/> -->

@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Detail Wakaf</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Detail Wakaf </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <div class="expns-info">
																																				<a href="#" title=""><center><img src="{{asset('assets/images/campaign/'.$data['gambar'])}}" alt="expns-img1.jpg" style="width:100%;max-width:500px;" ></center></a>
																																				<hr>
                                        <h4 style="text-align:center"><a href="#" title="">{{$data->judul}}</a></h4>
                                        <p style="text-align:center">{!! nl2br($data->isi) !!}</p> <br>
																																								<div class="expns-info-innr">
                                            <span>Dibutuhkan<i> {{number_format($data['target'],0,',','.')}} Paket</i></span>																																												
                                            <span>Terkumpul<i> {{number_format($terkumpul,0,',','.')}} Paket</i></span>
                                        </div>
																																								<br>
																																								<div class="row">
																																											<div class="col-md-12">
																																											<?php
																																														$target = $data['target'];
																																														$persen = $terkumpul/$target*100;
																																														$kumulasi = 0;
																																											?>
																																													<div class="progress">
																																															<div class="progress-bar" role="progressbar" style="width: {{number_format($persen,0,',','.')}}%;" aria-valuenow="{{number_format($persen,0,',','.')}}" aria-valuemin="0" aria-valuemax="100">{{number_format($persen,2,',','.')}}%</div>
																																													</div>
																																											</div>
																																											<hr>

																																								
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
																				</div>
																				<div class="table-responsive-md header-fixed" id="laporan">
																																								<table class="table table-bordered table-striped">
																																												<thead>
																																												<tr>
																																												<th width="40px;">No</th><th width="300px;">Donatur</th><th width="150px;">Jumlah Paket</th><th width="180px;">Kumulasi Paket</th><th width="150px;">Sisa Paket</th>
																																												@if(Auth::check())
																																												@if(Auth::user()->tipe_user_id == 1)
																																												<th width="180px;">Action</th>
																																												@endif
																																												@endif
																																												</tr>
																																												</thead>
																																												<tbody>

																																												@foreach($laporans as $key => $value)
																																												<?php $kumulasi = $kumulasi+$value->jumlah_paket;
																																												$target = $target-$value->jumlah_paket;?>
																																												<tr>
																																												<td width="40px;">{{$key+1}}.</td><td width="300px;">{{$value->nama}}</td><td width="150px;">{{number_format($value->jumlah_paket,0,',','.')}}</td><td width="180px;">{{number_format($kumulasi,0,',','.')}}</td><td width="150px;">{{number_format($target,0,',','.')}}</td>
																																												@if(Auth::check())
																																												@if(Auth::user()->tipe_user_id == 1)
																																												<td width="180px;"><a href="{{route('edit-wakaf',$value->id)}}" class="btn btn-warning">Edit</a> <button @click="hapus({{$value->id}})" class="btn btn-danger">Hapus</button></td>
																																												@endif
																																												@endif
																																												</tr>
																																												@endforeach
																																												</tbody>
																																												</table>
																																								</div>
                </div>
            </div>
        </section>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var laporan = new Vue({
	el:"#laporan",
	data: {
			nama:'',
			email:'',
			password:'',
			selected:'',
			options:[]
	},
	methods: {
		loading(text){
			Swal.fire({
									title: 'Mohon menunggu...',
									text:text,
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
		},
		hapus(id){
			Swal.fire({
					title: 'Yakin dihapus ?',
					text: "Data akan dihapus permanen!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Hapus Sekarang!',
					cancelButtonText: 'Nanti'
			}).then((result) => {
					if (result.isConfirmed) {
						this.lanjutkanHapus(id)							
					}
			})
		},
		async lanjutkanHapus(id){
			this.loading('Sedang dihapus...');
			let url = "<?php echo route('input-laporan'); ?>";
			let request = {ids:id, action:'hapus'};
			request['token'] = document.querySelector('#token').getAttribute('value');
			await axios.post(url,request).then((response) =>{
					swal(response.data.title, response.data.message, response.data.icon);
					if (response.data.code === 200) {
						location.reload();
					}
					Swal.close()									
			},(response)=>{
							swal("Gagal!", "GAGAL", "error");
							Swal.close()
			});
		}
	},
});
	</script>
@endsection