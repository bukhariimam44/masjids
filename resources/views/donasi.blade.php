@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="assets/images/resources/page-title-ayat.png" alt="page-title-ayat.png"></h1> -->
                        <h2>Donasi</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Donasi</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
																								@foreach($datas as $key=> $data)
																								<?php $jumlah_karakter = strlen($data['judul']);?>
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="expns-box">
                                    <a href="#" title=""><img src="{{asset('assets/images/campaign/'.$data['gambar'])}}" alt="expns-img1.jpg"></a>
                                    <div class="expns-info">
                                        <h4><a href="#" title="">{{substr($data['judul'], 0, 20)}} @if($jumlah_karakter > 20)... @endif</a></h4>
                                        <p>{!! substr($data['isi'], 0, 100) !!} ...</p>
                                        <div class="expns-info-innr">
                                            <span>Dibutuhkan<i> {{number_format($data['target'],0,',','.')}} Paket</i></span>																																												
                                            <span>Terkumpul<i> {{number_format($data['terkumpul'],0,',','.')}} Paket</i></span>
                                        </div>
																																								<br>
																																								<div class="row">
																																											<div class="col-md-12">
																																											<?php
																																														$target = $data['target'];
																																														$persen = $data['terkumpul']/$target*100;
																																											?>
																																													<div class="progress">
																																															<div class="progress-bar" role="progressbar" style="width: {{number_format($persen,0,',','.')}}%;" aria-valuenow="{{number_format($persen,0,',','.')}}" aria-valuemin="0" aria-valuemax="100">{{number_format($persen,2,',','.')}}%</div>
																																													</div>
																																											</div>
																																								</div>
																																								<br>
                                        <a class="thm-btn brd-rd40" href="{{route('donasi-sekarang',$data['id'])}}" title="">Donasi Sekarang</a>
                                    </div>
                                </div>
                            </div>

																												@endforeach
                            {{--<div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="expns-box">
                                    <a href="#" title=""><img src="assets/images/resources/expns-img1.jpg" alt="expns-img1.jpg"></a>
                                    <div class="expns-info">
                                        <h4><a href="#" title="">Mosque Renovation</a></h4>
                                        <p>Mosque renovations required your money to complete rooms accomodation.</p>
                                        <div class="expns-info-innr">
                                            <span>Funds Neded<i>5000.00 USD</i></span>
                                            <div class="expns-prg" id="expns-prg1"></div>
                                            <span>Still Required<i>2539.00 USD</i></span>
                                        </div>
                                        <a class="thm-btn brd-rd40" href="#" title="">Donasi Sekarang</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="expns-box">
                                    <a href="#" title=""><img src="assets/images/resources/expns-img2.jpg" alt="expns-img2.jpg"></a>
                                    <div class="expns-info">
                                        <h4><a href="#" title="">New Classes For Students</a></h4>
                                        <p>Classes renovations required your money to complete rooms accomodation.</p>
                                        <div class="expns-info-innr">
                                            <span>Funds Neded<i>7500.00 USD</i></span>
                                            <div class="expns-prg" id="expns-prg2"></div>
                                            <span>Still Required<i>3599.00 USD</i></span>
                                        </div>
                                        <a class="thm-btn brd-rd40" href="#" title="">Donasi Sekarang</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="expns-box">
                                    <a href="#" title=""><img src="assets/images/resources/expns-img3.jpg" alt="expns-img3.jpg"></a>
                                    <div class="expns-info">
                                        <h4><a href="#" title="">Helping Syrian Refugees</a></h4>
                                        <p>Helping Refugees required your money to complete rooms accomodation.</p>
                                        <div class="expns-info-innr">
                                            <span>Funds Neded<i>7500.00 USD</i></span>
                                            <div class="expns-prg" id="expns-prg3"></div>
                                            <span>Still Required<i>3599.00 USD</i></span>
                                        </div>
                                        <a class="thm-btn brd-rd40" href="#" title="">Donasi Sekarang</a>
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection