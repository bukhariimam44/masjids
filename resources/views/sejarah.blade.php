@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="assets/images/resources/page-title-ayat.png" alt="page-title-ayat.png"></h1> -->
                        <h2>Sejarah</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Sejarah</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
								<section>
            <div class="gap">
                <div class="container">
                    <div class="hstry-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-lg-6">
                                <div class="hstry-img text-center">
                                    <img src="{{asset('assets/images/alazzam.jpeg')}}" alt="AL-AZZAM">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-lg-6">
                                <div class="hstry-desc">
                                    <span>BEBERAPA KATA TENTANG KAMI</span>
                                    <h2>Sejarah Pusat</h2>
                                    <p>Sejak 1987, institusi kami membimbing pelajar dan orang-orang di seluruh dunia. semua gagasan yang salah tentang mencela kesenangan dan akan memberi Anda penjelasan lengkap. Sejak 1987, institusi kami membimbing pelajar dan orang-orang di seluruh dunia.</p>
                                    <strong>AL-AZZAM <span>#1</span> Islamic Center in the <span>Country!</span></strong>
                                    <ul>
                                        <li>Astonisihing Facilities</li>
                                        <li>Helping All Communities</li>
                                        <li>Leads Charity Events</li>
                                        <li>Schooling Children</li>
										<li>Feeding Hungry People</li>
                                        <li>Providing Accomodations</li>
										<li>Funding Poor People</li>
                                        <li>Providing Food & Clothes</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div><!-- History Wrap -->
                </div>
            </div>
        </section>
       
        <section>
            <div class="gap no-gap">
                <div class="container">
                    <div class="contr-wrap text-center">
                        <div class="contr-inner">
                            <div class="contr-desc contr-inr">
                                <h2>CARA BERKONTRIBUSI - GAGASAN</h2>
                                <p>Kami mendapat tanggapan yang luar biasa dari banyak saudara dan saudari tentang gagasan dan cara menghasilkan uang untuk tujuan mulia ini. Jika Anda memiliki gambaran tentang bagaimana dana dapat dikumpulkan, silakan hubungi kami.</p>
                            </div>
                            <div class="contr-butn contr-inr">
                                <a class="secndry-btn brd-rd40" href="{{route('kontak')}}" title="">HUBUNGI KAMI</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection