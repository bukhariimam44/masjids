@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(../assets/images/parallax13.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="assets/images/resources/page-title-ayat.png" alt="page-title-ayat.png"></h1> -->
                        <h2>Detail Berita</h2>
                        <ul class="breadcrumbs">
                            <li><a href="index.html" title="">Beranda</a></li>
                            <li><a href="blog.html" title="">Berita</a></li>
                            <li>Detail Berita</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap">
                <div class="container">
                    <div class="blog-detail-wrp">
                        <div class="row">
                            <div class="col-md-9 col-sm-12 col-lg-9">
                                <div class="blog-detail">
                                    <div class="blog-detail-inf brd-rd5">
                                        <img src="{{asset('assets/images/berita/'.$beritas->gambar)}}" alt="blog-detail-img.jpg">
                                        <div class="blog-detail-inf-inr">
                                            <h4>{{$beritas->judul}}</h4>
                                            <ul class="pst-mta">
                                                <li><i class="fa fa-calendar-o thm-clr"></i>{{date('d M Y',strtotime($beritas->created_at))}}</li>
                                                <li><i class="fa fa-user-o thm-clr"></i>{{$beritas->userId->name}}</li>
                                                <li><i class="fa fa-clock-o thm-clr"></i>{{date('D',strtotime($beritas->created_at))}}, {{date('H:i',strtotime($beritas->created_at))}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="blog-detail-desc">
                                        <p>{!! nl2br($beritas->isi) !!}</p>
                                        <!-- <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                                        <blockquote class="brd-rd5"><p><i class="fa fa-quote-left thm-clr"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></blockquote>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p> -->
                                        <div class="pst-shr-tgs">
                                            <div class="team-scl float-left">
                                                <span>Bagikan :</span>
                                                <!-- <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a> -->
                                                <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                                <!-- <a href="#" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a> -->
                                            </div>
                                            {{--<div class="tag-clouds float-right">
                                                <span>Tags:</span>
                                                <a href="#" title="">Namaz</a>, <a href="#" title="">Roza</a>, <a href="#" title="">Hajj</a>, <a href="#" title="">Zakat</a>
                                            </div>--}}
                                        </div>
                                        {{--<div class="cmts-wrp">
                                            <h3>02 Comments</h3>
                                            <ul class="cmt-thrd">
                                                <li>
                                                    <div class="cmt-bx">
                                                        <img class="brd-rd50" src="{{asset('assets/images/resources/cmt-img1.jpg')}}" alt="cmt-img1.jpg">
                                                        <div class="cmt-inf">
                                                            <h6>Mike Stepliton</h6>
                                                            <span>Aug 14, 2018</span>
                                                            <p itemprop="description">Similique sunt in culpa qui officia.vero eos et accusamus et iusto odio dignissimos ducimuss qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores</p>
                                                            <a class="comment-reply-link thm-clr" href="#" title="">Reply</a>
                                                        </div>
                                                    </div>
                                                    <ul class="children">
                                                        <li>
                                                            <div class="cmt-bx">
                                                                <img class="brd-rd50" src="{{asset('assets/images/resources/cmt-img2.jpg')}}" alt="cmt-img2.jpg">
                                                                <div class="cmt-inf">
                                                                    <h6>Maria Stepliton</h6>
                                                                    <span>Aug 14, 2018</span>
                                                                    <p>Similique sunt in culpa qui officia.vero eos et accusamus et iusto odio dignissismos ducimuss qui blanditiis praesentium voluptatum.</p>
                                                                    <a class="comment-reply-link thm-clr" href="#" title="">Reply</a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul><!-- Comment Thread -->
                                        </div><!-- Comments Wrap -->
                                        <div class="cnt-frm cmt-frm">
                                            <h3>Leave Your Comments</h3>
                                            <form>
                                                <div class="row mrg20">
                                                    <div class="col-md-4 col-sm-6 col-lg-4">
                                                        <input type="text" placeholder="Name">
                                                    </div>
                                                    <div class="col-md-4 col-sm-6 col-lg-4">
                                                        <input type="email" placeholder="Email">
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 col-lg-4">
                                                        <input type="text" placeholder="Subject">
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-lg-12">
                                                        <textarea placeholder="Message"></textarea>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-lg-12">
                                                        <button type="submit" class="thm-btn brd-rd40">POST COMMENT</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <div class="sidebar-wrp">
                                    <div class="wdgt-box">
                                        <h4>Pencarian</h4>
                                        <form class="srch-frm">
                                            <input type="text" placeholder="Cari Berita">
                                            <button type="submit" class="thm-clr"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                    {{--<div class="wdgt-box">
                                        <h4>Kategori</h4>
                                        <ul class="cat-lst">
                                            <li><a href="#" title="">House Accommodation</a>(10)</li>
                                            <li><a href="#" title="">Health Events</a>(06)</li>
                                            <li><a href="#" title="">Donation Event</a>(03)</li>
                                            <li><a href="#" title="">Education</a>(07)</li>
                                            <li><a href="#" title="">Food</a>(12)</li>
                                            <li><a href="#" title=""">Health Care</a>(02)</li>
                                        </ul>
                                    </div>--}}
                                    <div class="wdgt-box">
                                        <h4>Berita Terbaru</h4>
                                        <div class="rcnt-wrp">
																																												@foreach($berita as $value)
																																												<?php $jumlah_karakter = strlen($value->judul);
																																								?>
                                            <div class="rcnt-bx">
                                                <a href="{{route('detail-berita',$value->judul)}}" title=""><img src="{{asset('assets/images/berita/'.$value->gambar)}}" alt="{{$value->judul}}" style="width:60px;height:60px;"></a>
                                                <div class="rcnt-inf">
                                                    <h6><a href="{{route('detail-berita',$value->judul)}}" title="">{{substr($value->judul, 0, 20)}} @if($jumlah_karakter > 20)... @endif</a></h6>
                                                    <span class="thm-clr"><i class="fa fa-calendar-o"></i>{{date('d M Y',strtotime($value->updated_at))}}</span>
                                                </div>
                                            </div>
																																												@endforeach
                                            {{--<div class="rcnt-bx">
                                                <a href="blog-detail.html" title=""><img src="{{asset('assets/images/resources/rcnt-img2.jpg')}}" alt="rcnt-img2.jpg"></a>
                                                <div class="rcnt-inf">
                                                    <h6><a href="blog-detail.html" title="">Lorem ipsum dolor sit</a></h6>
                                                    <span class="thm-clr"><i class="fa fa-calendar-o"></i>May 25, 2020</span>
                                                </div>
                                            </div>
                                            <div class="rcnt-bx">
                                                <a href="blog-detail.html" title=""><img src="{{asset('assets/images/resources/rcnt-img3.jpg')}}" alt="rcnt-img3.jpg"></a>
                                                <div class="rcnt-inf">
                                                    <h6><a href="blog-detail.html" title="">Lorem ipsum dolor sit</a></h6>
                                                    <span class="thm-clr"><i class="fa fa-calendar-o"></i>Apr 15, 2020</span>
                                                </div>
                                            </div>
                                            <div class="rcnt-bx">
                                                <a href="blog-detail.html" title=""><img src="{{asset('assets/images/resources/rcnt-img4.jpg')}}" alt="rcnt-img4.jpg"></a>
                                                <div class="rcnt-inf">
                                                    <h6><a href="blog-detail.html" title="">Lorem ipsum dolor sit</a></h6>
                                                    <span class="thm-clr"><i class="fa fa-calendar-o"></i>Mar 12, 2020</span>
                                                </div>
                                            </div>--}}
                                        </div>
                                    </div>
                                </div><!-- Sidebar Wrap -->
                            </div>
                        </div>
                    </div><!-- Blog Detail Wrap -->
                </div>
            </div>
        </section>
@endsection