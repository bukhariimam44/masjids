@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax14.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="assets/images/resources/page-title-ayat.png" alt="page-title-ayat.png"></h1> -->
                        <h2>Kontak</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Kontak</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap">
                <div class="container">
                    <div class="sec-title text-center">
                        <div class="sec-title-inner">
                            <!-- <span>Get information</span> -->
                            <h3>Informasi kontak</h3>
                        </div>
                    </div>
                    <div class="contact-info-wrap text-center remove-ext3">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="contact-info-box">
                                    <i class="flaticon-location-pin"></i>
                                    <strong>Lokasi</strong>
                                    <span>Taman Cilegon Indah, Blok H12 No-1, Sukmajaya, Kec. Jombang, Cilegon - Banten 42416</span>
                                    <!-- <a href="#" title="">info@example.com</a> -->
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="contact-info-box">
                                    <i class="flaticon-call"></i>
                                    <strong>Kontak SMS/WA</strong>
                                    <span>(Ikhwan) : 0813 1139 9031</span>
                                    <span>(Akhwat) : 0819 3271 7985</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-lg-4">
                                <div class="contact-info-box">
                                    <i class="flaticon-email"></i>
                                    <strong>Rekening Donasi</strong>
                                    <a href="#" title="">Bank : Bank Syariah Mandiri</a>
                                    <a href="#" title="">No Rekening : 7145 555 714</a>
																																				<a href="#" title="">Atas Nama : Yayasan Al-Azzam Al-Mubarok (KB : 451)</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- Contact Info Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap no-gap">
												<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3967.8484302162824!2d106.07940251446388!3d-6.0155383607569615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e418dea01af3a43%3A0x1fb8191cc4995a38!2sTaman%20Cilegon%20Indah!5e0!3m2!1sen!2sid!4v1608565591002!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <!-- <div class="contact-loc" id="contact-loc"></div> -->
            </div>
        </section>
        <section>
            <div class="gap">
                <div class="container">
                    <div class="sec-title text-center">
                        <div class="sec-title-inner">
                            <!-- <span>Have Question</span> -->
                            <h3>Kirim Pesan</h3>
                        </div>
                    </div>
                    <div class="contact-form text-center">
                        <form>
                            <div class="row mrg20">
                                <div class="col-md-6 col-sm-6 col-lg-6">
                                    <input type="text" placeholder="Nama Lengkap">
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-6">
                                    <input type="text" placeholder="No HP/WA">
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-6">
                                    <input type="email" placeholder="Email">
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-6">
                                    <input type="text" placeholder="Judul">
                                </div>
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <textarea placeholder="Isi Pesan"></textarea>
                                    <button class="thm-btn brd-rd40" type="submit">Proses Kirim Pesan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
@endsection