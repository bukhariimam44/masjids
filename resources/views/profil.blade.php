@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax12.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="assets/images/resources/page-title-ayat.png" alt="page-title-ayat.png"></h1> -->
                        <h2>Profil</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Profil</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
								<section>
            <div class="gap">
                <div class="container">
                    <div class="sec-title style2 text-center">
                        <div class="sec-title-inner">
                            <h3><span class="secndry-clr">Things</span> We Are Doing</h3>
                        </div>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis um voluptatum deleniti atque orrupti quos dolores et quas molestias. Excepturi sint occaecati cupiditate.</p>
                    </div>
                    <div class="srv-wrap3">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-lg-4">
                                <div class="srv-mckp">
                                    <img src="assets/images/resources/srv-img.png" alt="srv-img.png">
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 col-lg-8">
                                <div class="remove-ext5">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="srv-box3">
                                                <div class="srv-ttl">
                                                    <i class="flaticon-quran-rehal"></i>
                                                    <h4>Quran Learning</h4>
                                                    <span>Teaching Quran Ayah</span>
                                                </div>
                                                <p>At vero eos et accusamus et iusto odio disimos ducimus qui blanditiis um voluptatum deleniti atque orrupti qlores et...</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="srv-box3">
                                                <div class="srv-ttl">
                                                    <i class="flaticon-heart-1"></i>
                                                    <h4>Charity Service</h4>
                                                    <span>Noble Cause To Help</span>
                                                </div>
                                                <p>At vero eos et accusamus et iusto odio disimos ducimus qui blanditiis um voluptatum deleniti atque orrupti qlores et...</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="srv-box3">
                                                <div class="srv-ttl">
                                                    <i class="flaticon-mosque"></i>
                                                    <h4>Mosque Development</h4>
                                                    <span>Renovating Mosques Everywhere</span>
                                                </div>
                                                <p>At vero eos et accusamus et iusto odio disimos ducimus qui blanditiis um voluptatum deleniti atque orrupti qlores et...</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="srv-box3">
                                                <div class="srv-ttl">
                                                    <i class="flaticon-social-care"></i>
                                                    <h4>Help Poor's</h4>
                                                    <span>Feeding The Hungry</span>
                                                </div>
                                                <p>At vero eos et accusamus et iusto odio disimos ducimus qui blanditiis um voluptatum deleniti atque orrupti qlores et...</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="gap gray-bg2">
                <div class="container">
                    <div class="msn-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-lg-6">
                                <div class="msn-thmb-wrap">
                                    <a href="{{asset('assets/images/alazzam.jpeg')}}" data-fancybox="gallery" title=""><img src="{{asset('assets/images/alazzam.jpeg')}}" alt="msn-img1.jpg"></a> 
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-lg-6">
                                <div class="msn-desc">
                                    <h3>Mission Statement</h3>
                                    <p>Islam is monotheistic religion teaching that there is only one incomparable God – Allah and that Muhammad is the messenger of God. It is the world’s second-largest religion and the fastest-growing major religion in the world. Islam teaches that God is merciful, all-powerful and unique.</p>
                                    <ul>
                                        <li>Nam facilisis mauris a metus finibus id gravida</li>
                                        <li>Cras neque tortor, faucibus sit amet amet lacus</li>
                                        <li>Fusce condimentum sem enim, est ornare ex vestibulum ut</li>
                                    </ul>
                                    <p>Islam is monotheistic religion teaching that there is only one incomparable God – Allah and that Muhammad is the messenger of God. It is the world’s second-largest religion and the fastest-growing.</p>
                                    <a class="thm-btn brd-rd40" href="#" title="">MORE ABOUT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection