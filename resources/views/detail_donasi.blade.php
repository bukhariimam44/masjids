@extends('layouts.app')
@section('css')
<style>
table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 100%;
  table-layout: fixed;
}

table caption {
  font-size: 1.5em;
  margin: .5em 0 .75em;
}

table tr {
  background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .35em;
}

table th,
table td {
  padding: .625em;
  text-align: left;
}

table th {
  font-size: .85em;
  letter-spacing: .1em;
  text-transform: uppercase;
}
</style>
@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(../assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Cara Donasi</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Cara Donasi </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <div class="expns-info">
																																				<a href="#" title=""><center><img src="{{asset('assets/images/campaign/'.$data['gambar'])}}" alt="expns-img1.jpg" style="width:100%;max-width:500px;" ></center></a>
																																				<hr>
                                        <h4 style="text-align:center"><a href="#" title="">{{$data->judul}}</a></h4>
                                        <p style="text-align:center">{!! nl2br($data->isi) !!}</p> <br>
																																								<div class="expns-info-innr">
                                            <span>Dibutuhkan<i> {{number_format($data['target'],0,',','.')}} Paket</i></span>																																												
                                            <span>Terkumpul<i> {{number_format($terkumpul,0,',','.')}} Paket</i></span>
                                        </div>
																																								<br>
																																								<div class="row">
																																											<div class="col-md-12">
																																											<?php
																																														$target = $data['target'];
																																														$persen = $terkumpul/$target*100;
																																											?>
																																													<div class="progress">
																																															<div class="progress-bar" role="progressbar" style="width: {{number_format($persen,0,',','.')}}%;" aria-valuenow="{{number_format($persen,0,',','.')}}" aria-valuemin="0" aria-valuemax="100">{{number_format($persen,2,',','.')}}%</div>
																																													</div>
																																											</div>
																																											<hr>

																																								<div class="table-responsive">
																																								<table class="table">
																																												<thead>
																																												<th scope="col" width="30px;"></th><th scope="col">Tata cara donasi</th>
																																												</thead>
																																												<tbody>
																																												
																																												<tr>
																																												<td>1.</td><td>1 Paket seharga Rp 100.000</td>
																																												</tr>
																																												<tr>
																																												<td>2.</td><td>Melakukan transfer sesuai jumlah harga paket yang di ambil</td>
																																												</tr>
																																												<tr>
																																												<td>3.</td><td>Transfer ke Rekening <br>
																																												* Bank Mandiri Syariah <br>* Kode Bank 451 <br>* No. Rek : 7145 5557 14 <br>* An : Yayasan Al-Azzam Al-Mubarok</td>
																																												</tr>
																																												<tr>
																																												<td>4.</td><td>Setelah Transfer berhasil klik tombol konfirmasi transfer</td>
																																												</tr>
																																												</tbody>
																																												</table>
																																												<a href="{{route('konfirmasi-transfer',$data->id)}}" class="thm-btn brd-rd40">Konfirmasi Transfer</a>
																																								</div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('js')
@endsection