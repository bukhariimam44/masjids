@extends('layouts.app')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Add User</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Add User </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
								<section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <div class="expns-info">
																		
																																						<div class="container">
                   
																																										<div class="contact-form text-center">
																																														<form id="add_user">
																																																		<div class="row mrg20">
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																						<input type="text" v-model="nama" placeholder="Nama" required>     
																																																						</div>
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																						<input type="email" v-model="email" placeholder="Email" required>     
																																																						</div>
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																						<input type="password" v-model="password" placeholder="Password" required>     
																																																						</div>
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																						<select v-model="selected" id="" required>
																																																							<option value="">Akses Sebagai</option>
																																																							<option :value="item.id" v-for="(item, index) in options" :key="index">@{{item.tipe}}</option>
																																																						
																																																						</select>   
																																																						</div>
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																										<br><br>
																																																										<a class="thm-btn brd-rd40" @click="checkForm()">SIMPAN</a>
																																																						</div>
																																																		</div>
																																														</form>
																																										</div>
																																						</div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var add_user = new Vue({
	el:"#add_user",
	data: {
		nama:'',
			email:'',
			password:'',
			selected:'',
			options:[]
	},
	mounted() {
		this.load()
	},
	methods: {
		async load () {
			let urloptions = "<?php echo route('getTipe'); ?>";
					await axios.get(urloptions).then((response) =>{
							this.options = response.data.data
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});      
    },
		loading(text){
			Swal.fire({
									title: 'Mohon menunggu...',
									text:text,
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
		},
		async checkForm() {
				this.loading('Sedang dikirim...');
				if (this.nama < 3) {
						swal("Opss...", "Nama minimal 3 Digit.", "error");
						Swal.close()
				}else if(this.email < 6) {
						swal("Opss...", "Email minimal 6 Digit ", "error");
						Swal.close()
					}else if(this.password < 6) {
						swal("Opss...", "Password minimal 6 Digit ", "error");
						Swal.close()
					}else if(!this.selected) {
						swal("Opss...", "Akses belum dipilih ", "error");
						Swal.close()
				}else{
					let url = "<?php echo route('add-user'); ?>";
					let request = {email:this.email, password: this.password, nama : this.nama, tipe: this.selected, action:'add'};
					request['token'] = document.querySelector('#token').getAttribute('value');
					await axios.post(url,request).then((response) =>{
							swal(response.data.title, response.data.message, response.data.icon);
							if (response.data.code === 200) {
								window.location.href = "<?php echo route('data-user'); ?>";
							}
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});
				}
		},
	}
});
</script>
@endsection