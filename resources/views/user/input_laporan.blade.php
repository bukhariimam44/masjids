@extends('layouts.app')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 100%;
  table-layout: fixed;
}

table caption {
  font-size: 1.5em;
  margin: .5em 0 .75em;
}

table tr {
  background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .35em;
}

table th,
table td {
  padding: .625em;
  text-align: left;
}

table th {
  font-size: .85em;
  letter-spacing: .1em;
  text-transform: uppercase;
}
</style>
@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(../assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Input Laporan Wakaf</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Input Laporan</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap remove-gap" id="input_laporan">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <div class="expns-info">
																																	
																																								<div class="container">
                    <div class="sec-title text-center">
                        <div class="sec-title-inner">
                            <!-- <span>Have Question</span> -->
                            <h5>Form Laporan Wakaf</h5>
                        </div>
                    </div>
                    <div class="contact-form text-center">
                        <form action="#">
                            <div class="row mrg20">
                                <div class="col-md-12 col-sm-12 col-lg-12">
																																<select v-model="selected">
																																<option value="0">Wakaf Untuk ?</option>
																																<option :value="item.id" v-for="(item, index) in juduls" :key="index">@{{item.judul}}</option>
																																</select>
                                </div>
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <input type="text" v-model="nama" placeholder="Nama Lengkap">
                                </div>
																																<div class="col-md-12 col-sm-12 col-lg-12">
                                    <input type="number" v-model="jumlah_paket" placeholder="Jumlah Paket">
                                </div>
      
																																<div class="col-md-12 col-sm-12 col-lg-12">
																																<img
																																		:src="imgCaptcha"
																																		alt=""
																																>
																																<input
																																		v-model="captcha"
																																		placeholder="Koda Captcha. . . . . . . . . . . . . . . . . ."
																																		type="text"
																																		class="form-control mt-2"
																																>
                                </div>
																																<div class="col-md-12 col-sm-12 col-lg-12">
																																
																																				<br><br>
                                    <a class="thm-btn brd-rd40" href="#" @click="checkForm()">SIMPAN DATA</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var input_laporan = new Vue({
	el:"#input_laporan",
	data: {
			selected:0,
			juduls:[],
			nama:'',
			jumlah_paket:'',
			captcha:'',
			imgCaptcha:'',
			key:''
	},
	mounted() {
		this.refreshCaptcha()
		this.load()
	},
	methods: {
		async refreshCaptcha () {
			let urlcaptcha = "<?php echo url('api/get-captcha/flat'); ?>";
					await axios.get(urlcaptcha).then((response) =>{
							this.imgCaptcha = response.data.img
							this.key = response.data.key
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});      
    },
				async load () {
			let url = "<?php echo route('get-campaign'); ?>";
					await axios.get(url).then((response) =>{
							this.juduls = response.data.data
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});      
    },
		loading(text){
			Swal.fire({
									title: 'Mohon menunggu...',
									text:text,
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
		},
		async checkForm() {
				this.loading('Sedang dikirim...');
				if (this.selected < 1) {
						swal("Opss...", "Pilih penggalangan dana", "error");
						Swal.close()
				}else if(this.nama.length < 3) {
						swal("Opss...", "Nama Lengkap minimal 3 Digit ", "error");
						Swal.close()
				}else if (this.jumlah_paket < 1) {
						swal("Opss...", "Jumlah Paket minimal 1. ", "error");
						Swal.close()
				}else if (this.captcha.length < 6) {
						swal("Opss...", "Captcha minimal 6 karakter. ", "error");
						Swal.close()
				}else{
					this.refreshCaptcha()
					let url = "<?php echo route('input-laporan'); ?>";
					let request = {campaign_id:this.selected, nama:this.nama, jumlah_paket:this.jumlah_paket, captcha : this.captcha, key: this.key, action:'add'};
					request['token'] = document.querySelector('#token').getAttribute('value');
					await axios.post(url,request).then((response) =>{
							swal(response.data.title, response.data.message, response.data.icon);
							if (response.data.code === 200) {
								window.location.href = "<?php echo url('detail-laporan-wakaf-'); ?>"+this.selected;
								this.selected = 0;
								this.nama = '';
								this.jumlah_paket = '';
								this.captcha = '';
							}
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});
				}
		},
	}
});
</script>
@endsection