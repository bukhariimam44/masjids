@extends('layouts.app')

@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Home</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Home </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
<section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <div class="expns-info">
																		
																																								<div class="container">
																																												<div class="sec-title text-center">
																																																<div class="sec-title-inner">
																																																				<!-- <span>Have Question</span> -->
																																																				<h5>Selamat Datang...</h5>
																																																</div>
																																												</div>
																																												<div class="contact-form text-center">
																																																<h4 class="thm-clr">{{Auth::user()->name}}</h4> <br>
																																																<label>{{Auth::user()->email}}</label><br>
																																																<label>Login sebagai : {{Auth::user()->userTipe->tipe}}</label>
																																												</div>
																																								</div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
