@extends('layouts.app')
@section('css')
<style>
table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 100%;
  table-layout: fixed;
}

table caption {
  font-size: 1.5em;
  margin: .5em 0 .75em;
}

table tr {
  background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .35em;
}

table th,
table td {
  padding: .625em;
  text-align: left;
}

table th {
  font-size: .85em;
  letter-spacing: .1em;
  text-transform: uppercase;
}
</style>
@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Data User</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Data User </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <!-- <div class="expns-info"> -->
																	
																																								<div class="row">
																																										

																																								<div class="table-responsive">
																																									<a href="{{route('add-user')}}" class="thm-btn brd-rd40">Add</a>
																																								<table class="table table-bordered table-striped">
																																												<thead>
																																												<th scope="col" width="40px;">No</th><th scope="col" width="200px;">Nama</th><th scope="col" width="300px;">Email</th><th scope="col" width="200px;">Tipe User</th><th scope="col" width="200px;">Action</th>
																																												</thead>
																																												<tbody>

																																												@foreach($users as $key => $value)
																																												<tr>
																																												<td width="40px;">{{$key+1}}.</td><td width="200px;">{{$value->name}}</td><td width="300px;">{{$value->email}}</td><td width="200px;">{{$value->userTipe->tipe}}</td><td width="200px;"><a href="{{route('edit-user',$value->id)}}" class="btn btn-warning">Edit</a> @if($value->aktif == 1)<a href="{{route('nonaktif-user',$value->id)}}" class="btn btn-danger">Non Aktifkan</a>@else <a href="{{route('aktif-user',$value->id)}}" class="btn btn-success">Aktifkan</a> @endif</td>
																																												</tr>
																																												@endforeach
																																												</tbody>
																																												</table>
																																								</div>
                                            
                                        <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('js')
@endsection