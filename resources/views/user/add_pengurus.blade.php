@extends('layouts.app')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Add Pengurus</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Add Pengurus </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
								<section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <div class="expns-info">
																		
																																						<div class="container">
                   
																																										<div class="contact-form text-center">
																																														<form id="pengurus">
																																																		<div class="row mrg20">
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																						<input type="text" v-model="nama" placeholder="Nama" required>     
																																																						</div>
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																						<input type="file" accept="image/jpeg" @change="uploadImage" required>     
																																																						</div>
																																																						
																																																						<div class="col-md-12 col-sm-12 col-lg-12">
																																																										<br><br>
																																																										<button class="thm-btn brd-rd40" @click="checkForm()">SIMPAN</button>
																																																						</div>
																																																		</div>
																																														</form>
																																										</div>
																																						</div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var pengurus = new Vue({
	el:"#pengurus",
	data: {
		nama:'',
		foto:''
	},
	methods: {
		uploadImage(e){
						const image = e.target.files[0];
						const reader = new FileReader();
						reader.readAsDataURL(image);
						reader.onload = e =>{
										this.foto = e.target.result;
						};
		},
		loading(text){
			Swal.fire({
									title: 'Mohon menunggu...',
									text:text,
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
		},
		async checkForm() {
				this.loading('Sedang dikirim...');
				if (this.nama < 3) {
						swal("Opss...", "Nama minimal 3 Digit.", "error");
						Swal.close()
				}else if(!this.foto) {
						swal("Opss...", "Foto tidak boleh kosong ", "error");
						Swal.close()
				}else{
					let url = "<?php echo route('add-pengurus'); ?>";
					let request = {nama : this.nama, foto: this.foto, action:'add'};
					request['token'] = document.querySelector('#token').getAttribute('value');
					await axios.post(url,request).then((response) =>{
							swal(response.data.title, response.data.message, response.data.icon);
							if (response.data.code === 200) {
								window.location.href = "<?php echo route('pengurus'); ?>";
							}
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});
				}
		},
	}
});
</script>
@endsection