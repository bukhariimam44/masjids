@extends('layouts.app')
@section('css')
<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
<meta name="_token" id="token" value="{{csrf_token()}}">

<style>
table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 100%;
  table-layout: fixed;
}

table caption {
  font-size: 1.5em;
  margin: .5em 0 .75em;
}

table tr {
  background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .35em;
}

table th,
table td {
  padding: .625em;
  text-align: left;
}

table th {
  font-size: .85em;
  letter-spacing: .1em;
  text-transform: uppercase;
}
</style>
@endsection
@section('content')
@include('flash::message')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Pengajuan Transfer</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Pengajuan Transfer </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
        <section id="konfirmasi">
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <!-- <div class="expns-info"> -->
																	
																																								<div class="row">
																																										

																																								<div class="table-responsive">
																																								<table class="table table-bordered table-striped">
																																												<thead>
																																												<th scope="col" width="60px;">No</th><th scope="col">Tanggal & Wakaf</th><th scope="col">Nama & No HP</th><th scope="col">Jumlah Paket</th><th scope="col">Status</th><th scope="col">Action</th>
																																												</thead>
																																												<tbody>
																																												<tr v-for="(item, index) in datas" :key="index">
																																												<td>@{{index+1}}.</td><td>@{{item.tanggal}} <br>@{{item.campaign_id}}</td><td>@{{item.nama}} <br>@{{item.nohp}}</td><td>@{{item.jml_pkt}} Paket <br>Rp @{{formatPrice(item.jml_pkt*100000)}}</td><td>@{{item.status}} <br><button @click="bukti(item)" class="btn btn-success"> <i class="fa fa-search"></i> Bukti Transfer</button></td><td><button v-if="item.status_transfer_id === 1" @click="proses(item)" class="btn btn-primary">Proses</button> <button v-if="item.status_transfer_id === 1" @click="batal(item)" class="btn btn-danger">Batalkan</button></td>
																																												</tr>
																																												</tbody>
																																												</table>
																																								</div>
                                            
                                        <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>

<script>
// $("#files").change(function() {
//   filename = this.files[0].name
//   console.log(filename);
// });

var konfirmasi = new Vue({
	el:"#konfirmasi",
	data: {
		datas:[]
	},
	mounted() {
		this.load()
	},
	methods: {
		async load () {
			let url = "<?php echo route('getTransfer'); ?>";
					await axios.get(url).then((response) =>{
							this.datas = response.data.data
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});      
    },
		loading(text){
			Swal.fire({
									title: 'Mohon menunggu...',
									text:text,
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
		},
		formatPrice (value) {
				const val = (value / 1).toFixed(2).replace('.', ',')
				return (val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')).replace(',00', '')
		},
		bukti(item){
			Swal.fire({
					imageUrl: item.bukti_transfer,
					// imageHeight: 1500,
					imageAlt: 'A tall image'
			});
		},
		proses(item){
			Swal.fire({
					title: 'Yakin Proses ?',
					text: "Apakah uangnya sudah diterima ?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Proses Sekarang',
					cancelButtonText: 'Nanti'
			}).then((result) => {
					if (result.isConfirmed) {
							this.lanjutkanProses(item)
					}
			})
		},
		batal(item){
			Swal.fire({
					title: 'Yakin Dibatalkan ?',
					// text: "Apakah uangnya sudah diterima ?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Batalkan Sekarang',
					cancelButtonText: 'Nanti'
			}).then((result) => {
					if (result.isConfirmed) {
							this.lanjutkanBatal(item)
					}
			})
		},
		async lanjutkanProses(item){
				this.loading('Sedang diproses...');
				let url = "<?php echo route('pengajuan-transfer'); ?>";
				let request = {ids:item.id, action: 'proses'};
				request['token'] = document.querySelector('#token').getAttribute('value');
				await axios.post(url,request).then((response) =>{
						swal(response.data.title, response.data.message, response.data.icon);
						if (response.data.code === 200) {
							this.load();
						}
						Swal.close()		;							
				},(response)=>{
								swal("Gagal!", "GAGAL", "error");
								Swal.close();
				});

		},
		async lanjutkanBatal(item){
				this.loading('Sedang dibatalkan...');
				let url = "<?php echo route('pengajuan-transfer'); ?>";
				let request = {ids:item.id, action: 'batal'};
				request['token'] = document.querySelector('#token').getAttribute('value');
				await axios.post(url,request).then((response) =>{
						swal(response.data.title, response.data.message, response.data.icon);
						if (response.data.code === 200) {
							this.load();
						}
						Swal.close()		;							
				},(response)=>{
								swal("Gagal!", "GAGAL", "error");
								Swal.close();
				});

		}
		// async checkForm() {
		// 		this.loading('Sedang dikirim...');
		// 		if (this.nama < 3) {
		// 				swal("Opss...", "Nama minimal 3 Digit.", "error");
		// 				Swal.close()
		// 		}else if(this.email < 6) {
		// 				swal("Opss...", "Email minimal 6 Digit ", "error");
		// 				Swal.close()
		// 			}else if(this.password < 6) {
		// 				swal("Opss...", "Password minimal 6 Digit ", "error");
		// 				Swal.close()
		// 			}else if(!this.selected) {
		// 				swal("Opss...", "Akses belum dipilih ", "error");
		// 				Swal.close()
		// 		}else{
		// 			let url = "<?php echo route('add-user'); ?>";
		// 			let request = {email:this.email, password: this.password, nama : this.nama, tipe: this.selected};
		// 			request['token'] = document.querySelector('#token').getAttribute('value');
		// 			await axios.post(url,request).then((response) =>{
		// 					swal(response.data.title, response.data.message, response.data.icon);
		// 					if (response.data.code === 200) {
		// 						window.location.href = "<?php echo route('data-user'); ?>";
		// 					}
		// 					Swal.close()									
		// 			},(response)=>{
		// 							swal("Gagal!", "GAGAL", "error");
		// 							Swal.close()
		// 			});
		// 		}
		// },
	}
});
</script>
@endsection