@extends('layouts.app')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(../assets/images/resources/{{$data->bg}});"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <!-- <h1><img src="{{asset('assets/images/resources/dkms.jpg')}}" alt="page-title-ayat.png"></h1> -->
                        <h2>Kegiatan {{strtoupper($data->name)}}</h2>
                        <ul class="breadcrumbs">
                            <li><a href="{{route('index')}}" title="">Beranda</a></li>
                            <li>Kegiatan</li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
        </section>
		      <section>
            <div class="gap" id="kegiatan">
                <div class="container">
                    <div class="prtfl-wrap text-center">
                        <!-- <ul class="fltr-lnks">
                            <li class="active"><a data-filter="*" href="#">Semua</a></li>
                            <li><a data-filter=".fltr-itm1" href="#">Islamic Teaching</a></li>
                            <li><a data-filter=".fltr-itm2" href="#">Books</a></li>
                            <li><a data-filter=".fltr-itm3" href="#">Audio Books</a></li>
                            <li><a data-filter=".fltr-itm4" href="#">Videos</a></li>
                        </ul> -->
																								@if(Auth::check())
																										@if(Auth::user()->tipe_user_id == 1)
																											<button class="btn btn-success" onclick="document.getElementById('getFile').click()">Tambah Foto</button>
  																														<input type='file' id="getFile" accept="image/jpeg" style="display:none" @change=uploadImage>
																											<br><br>
																										@endif
																								@endif
                        <div class="prtfl-dta remove-ext1">
                            <div class="row mrg15 masonry">
																												@foreach($kegiatans as $key => $kegiatan)
                                <div class="col-md-4 col-sm-6 col-lg-4 fltr-itm fltr-itm1">
                                    <div class="prtfl-box">
                                        <img src="{{asset('assets/images/kegiatan/'.$kegiatan->foto)}}" alt="{{$kegiatan->judul}}">
                                        <div class="prtfl-info">
                                            <a href="{{asset('assets/images/kegiatan/'.$kegiatan->foto)}}" data-fancybox="gallery" title=""><i class="fa fa-plus"></i></a>
                                            <h4>{{$kegiatan->judul}}</h4>
                                            <!-- <span>artwork / branding</span> -->
                                        </div>
                                    </div>
                                </div>
																												@endforeach
																																{{--<div class="col-md-4 col-sm-6 col-lg-4 fltr-itm fltr-itm1">
                                    <div class="prtfl-box">
                                        <img src="{{asset('assets/images/resources/prtfl-img1.jpg')}}" alt="prtfl-img1.jpg">
                                        <div class="prtfl-info">
                                            <a href="{{asset('assets/images/resources/prtfl-img1.jpg')}}" data-fancybox="gallery" title=""><i class="fa fa-plus"></i></a>
                                            <h4>Islamic Center</h4>
                                            <span>artwork / branding</span>
                                        </div>
                                    </div>
                                </div>
																																<div class="col-md-4 col-sm-6 col-lg-4 fltr-itm fltr-itm1">
                                    <div class="prtfl-box">
                                        <img src="{{asset('assets/images/resources/prtfl-img1.jpg')}}" alt="prtfl-img1.jpg">
                                        <div class="prtfl-info">
                                            <a href="{{asset('assets/images/resources/prtfl-img1.jpg')}}" data-fancybox="gallery" title=""><i class="fa fa-plus"></i></a>
                                            <h4>Islamic Center</h4>
                                            <span>artwork / branding</span>
                                        </div>
                                    </div>
                                </div>
																																<div class="col-md-4 col-sm-6 col-lg-4 fltr-itm fltr-itm1">
                                    <div class="prtfl-box">
                                        <img src="{{asset('assets/images/resources/prtfl-img1.jpg')}}" alt="prtfl-img1.jpg">
                                        <div class="prtfl-info">
                                            <a href="{{asset('assets/images/resources/prtfl-img1.jpg')}}" data-fancybox="gallery" title=""><i class="fa fa-plus"></i></a>
                                            <h4>Islamic Center</h4>
                                            <span>artwork / branding</span>
                                        </div>
                                    </div>
                                </div>--}}                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var kegiatan = new Vue({
	el:"#kegiatan",
	data: {
			id:'{{$data->id}}',
			name:'{{$data->name}}',
			bukti:'',
			foto:''
	},
	methods: {
		uploadImage(e){
						const image = e.target.files[0];
						const reader = new FileReader();
						reader.readAsDataURL(image);
						reader.onload = e =>{
										this.bukti = 'Foto Terisi';
										this.foto = e.target.result;
										this.uploadKegiatan();
						};
		},
		loading(text){
			Swal.fire({
									title: 'Mohon menunggu...',
									text:text,
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
		},
		async uploadKegiatan() {
				this.loading('Sedang diupload...');
				let url = "<?php echo route('upload-kegiatan'); ?>";
				let request = {id:this.id,name:this.name, foto:this.foto};
				request['token'] = document.querySelector('#token').getAttribute('value');
				await axios.post(url,request).then((response) =>{
						swal(response.data.title, response.data.message, response.data.icon);
						if (response.data.code === 200) {
							this.foto = '';
							location.reload();
						}
						Swal.close()									
				},(response)=>{
								swal("Gagal!", "GAGAL", "error");
								Swal.close()
				});
		},
	}
});
</script>
@endsection