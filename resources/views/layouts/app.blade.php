<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
				<link rel="icon" href="{{asset('assets/images/favicon.png')}}" sizes="32x32" type="image/png">
    <title>{{ config('app.name', 'Laravel') }}</title>
				@yield('css')
    <link rel="stylesheet" href="{{asset('assets/css/icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fancybox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/jquery.circliful.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

    <!-- Color Scheme -->
    <link rel="stylesheet" href="{{asset('assets/css/colors/color.css')}}" title="color" /><!-- Color -->
    <link rel="alternate stylesheet" href="{{asset('assets/css/colors/color2.css')}}" title="color2" /> <!-- Color2 -->
    <link rel="alternate stylesheet" href="{{asset('assets/css/colors/color3.css')}}" title="color3" /> <!-- Color3 -->
    <link rel="alternate stylesheet" href="{{asset('assets/css/colors/color4.css')}}" title="color4" /> <!-- Color4 -->
</head>
<body>
<main>
        {{--<div class="pageloader-wrap">
            <div class="loader">
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__bar"></div>
                <div class="loader__ball"></div>
            </div>
        </div>--}}<!-- Pageloader Wrap -->
        <div class="sidepanel">
            <span><i class="fa fa-cog fa-spin"></i></span>
            <div class="color-picker">
                <h3>Choose Your Color</h3>
                <a class="color applied" onclick="setActiveStyleSheet('color'); return false;"></a>
                <a class="color2" onclick="setActiveStyleSheet('color2'); return false;"></a>
                <a class="color3" onclick="setActiveStyleSheet('color3'); return false;"></a>
                <a class="color4" onclick="setActiveStyleSheet('color4'); return false;"></a>
            </div><!-- Color Picker -->
        </div><!-- Side Panel -->
								@include('includes.header')
        @yield('content')
        @include('includes.footer')
    </main>
@yield('js')
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/downCount.js')}}"></script>
    <script src="{{asset('assets/js/fancybox.min.js')}}"></script>
    <script src="{{asset('assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/js/perfectscrollbar.min.js')}}"></script>
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.circliful.min.js')}}"></script>
    <script src="{{asset('assets/js/custom-scripts.js')}}"></script>
</body>
</html>
