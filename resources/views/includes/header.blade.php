<header class="style1">
            <div class="container">
                <div class="logo"><a href="{{route('index')}}" title="Logo"><img src="{{asset('assets/images/logo5.png')}}" alt="logo5.png" style="width:170px;"></a></div>
                <nav>
                    <div>
                        <a class="thm-btn brd-rd40" href="{{route('donasi')}}" title="">Donasi</a>
                        @include('includes.menu')
                    </div>
                </nav>
            </div>
								</header>
								<div class="rspn-hdr">
            <div class="rspn-mdbr">
                <ul class="rspn-scil">
                    <li><a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a></li>                   
                </ul>
                <form class="rspn-srch">
                    <input type="text" placeholder="Enter Your Keyword" />
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="lg-mn">
                <div class="logo"><a href="{{route('index')}}" title="Logo"><img src="{{asset('assets/images/logo5.png')}}" alt="logo5.png" style="width:170px;"></a></div>
                <div class="rspn-cnt">
                    <span><i class="fa fa-phone thm-clr"></i><a href="#" title="">0813 1139 9031 (Ikhwan)</a></span>
                    <span><i class="fa fa-phone thm-clr"></i>0819 3271 7985 (Akhwat)</span>
                </div>
                <span class="rspn-mnu-btn"><i class="fa fa-list-ul"></i></span>
            </div>
            <div class="rsnp-mnu">
                <span class="rspn-mnu-cls"><i class="fa fa-times"></i></span>
                @include('includes.menu')
            </div><!-- Responsive Menu -->
        </div>