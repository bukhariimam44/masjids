<footer>
            <div class="gap top-spac60 drk-bg remove-bottom">
                <div class="container">
                    <div class="footer-data">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-lg-4">
                                <div class="wdgt-box">
                                    <div class="logo"><a href="#" title="Logo"><img src="{{asset('assets/images/logo5.png')}}" alt="Logo" width="200px"></a></div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 col-lg-8">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="wdgt-box">
                                            <h4>Menu 1</h4>
                                            <ul>
                                                <li><a href="{{route('index')}}" title="">Beranda</a></li>
                                                <li><a href="#" title="">Profil</a></li>
                                                <li><a href="{{route('sejarah')}}" title="">Sejarah</a></li>
                                                <li><a href="{{route('pengurus')}}" title="">Pengurus</a></li>
																																																<li><a href="{{route('donasi')}}" title="">Donasi</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="wdgt-box">
                                            <h4>Menu 2</h4>
                                            <ul>
																																															<li><a href="{{route('berita')}}" title="">Berita</a></li>
                                                <li><a href="{{route('kegiatan','LTTQ')}}" title="">Kegiatan LTTQ</a></li>
                                                <li><a href="{{route('kegiatan','MAA')}}" title="">Kegiatan MAA</a></li>
                                                <li><a href="{{route('laporan-wakaf')}}" title="">Laporan Wakaf</a></li>
                                                <li><a href="#" title="">Laporan Infaq</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="wdgt-box">
                                            <h4>Kontak</h4>
                                            <ul class="cont-lst">
                                                <li><i class="flaticon-location-pin"></i>Taman Cilegon Indah, Blok H12 No-1, Sukmajaya, Kec. Jombang, Cilegon - Banten 42416</li>
                                                <li><i class="flaticon-call"></i>0813 1139 9031 (Ikhwan) </li>
                                                <li><i class="flaticon-call"></i><a href="#" title="">0819 3271 7985 (Akhwat)</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Footer Data -->
                    <div class="bottom-bar">
                        <p>&copy; Copyright 2020. All Rights Reserved.</p>
                        <div class="scl">
                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                            <a href="#" title="Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                            <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                        </div>
                    </div><!-- Bottom Bar -->
                </div>
            </div>
        </footer>