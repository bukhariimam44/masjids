<ul>
																												<li><a href="{{route('index')}}" title="">Beranda</a></li>
                            <!-- <li class="menu-item-has-children"><a href="#" title="">Home</a>
                                <ul>
                                    <li><a href="index.html" title="">Homepage 1</a></li>
                                    <li><a href="index2.html" title="">Homepage 2</a></li>
                                    <li><a href="index-simple.html" title="">Simple Homepage 1</a></li>
                                    <li><a href="index2-simple.html" title="">Simple Homepage 2</a></li>
																																				<li class="menu-item-has-children"><a href="#" title="">Header Styles</a>
                                        <ul>
                                            <li><a href="index.html" title="">Header Style 1</a></li>
                                            <li><a href="index2.html" title="">Header Style 2</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <li class="menu-item-has-children"><a href="#" title="">Tentang</a>
                                <ul>
                                    <li><a href="{{route('profil')}}" title="Profil">Profil</a></li>
																																				<li><a href="{{route('sejarah')}}" title="Sejarah">Sejarah</a></li>
																																				<li><a href="{{route('pengurus')}}" title="Pengurus">Pengurus</a></li>
                                </ul>
                            </li>
                            <li><a href="{{route('berita')}}" title="Berita">Berita</a></li>
																												<!-- <li><a href="#" title="">Jadwal Jumat</a></li> -->
																												<li class="menu-item-has-children"><a href="#" title="">Kegiatan</a>
                                <ul>
                                    <li><a href="{{route('kegiatan','LTTQ')}}" title="">LTTQ</a></li>
																																				<li><a href="{{route('kegiatan','MAA')}}" title="">MAA</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children"><a href="#" title="">Laporan</a>
																													<ul>
																														<li><a href="{{route('laporan-wakaf')}}" title="">Wakaf</a></li>
																														<!-- <li><a href="{{route('laporan-infaq')}}" title="">Infaq</a></li> -->
																													</ul>
																												</li>
                            <li><a href="{{route('kontak')}}" title="">Kontak</a></li>
																												@if(Auth::check())
																												@if(Auth::user()->tipe_user_id == 1)
																												<li class="menu-item-has-children"><a href="#" title="">Admin</a>
                                <ul>
																																				<li><a href="{{route('home')}}" title="">Home</a></li>
                                    <li><a href="{{route('data-user')}}" title="">Data User</a></li>
																																				<li><a href="{{route('galang-dana')}}" title="">Galang Dana</a></li>
																																				<!-- <li><a href="{{route('pengajuan-transfer')}}" title="">Pengajuan Transfer</a></li> -->
																																				<li><a href="{{route('input-laporan')}}" title="">Input Laporan</a></li>
																																				<li><a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" title="">Keluar</a></li>
                                </ul>
                            </li>
																												@elseif(Auth::user()->tipe_user_id == 2)
																												<li class="menu-item-has-children"><a href="#" title="">Petugas</a>
                                <ul>
																																<li><a href="{{route('input-laporan')}}" title="">Input Laporan</a></li>																																				
																																<li><a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" title="">Keluar</a></li>
                                </ul>
                            </li>
																												@endif
																												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
																												@else
																												<li><a href="{{route('login')}}" title="">Login</a></li>
																												@endif
                        </ul>