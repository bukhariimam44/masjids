@extends('layouts.app')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content')
<section>
            <div class="gap remove-bottom black-layer2 opc85">
                <div class="fixed-bg" style="background-image: url(assets/images/parallax8.jpg);"></div>
                <div class="container">
                    <div class="page-title-wrap">
                        <h2>Login</h2>
                        <ul class="breadcrumbs">
                            <li><a href="#" title="">Beranda</a></li>
                            <li>Login </li>
                        </ul>
                    </div><!-- Page Title Wrap -->
                </div>
            </div>
												<div class="container">
				{{--<div class="row justify-content-center"  style="margin-top:200px;margin-bottom:200px;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>--}}
</div>
        </section>
								<section>
            <div class="gap remove-gap">
                <div class="container">
                    <div class="expns-wrp remove-ext3">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="expns-box">
                                    
                                    <div class="expns-info">
																		
																																								<div class="container">
                    <div class="sec-title text-center">
                        <div class="sec-title-inner">
                            <!-- <span>Have Question</span> -->
                            <h5>Form Login</h5>
                        </div>
                    </div>
                    <div class="contact-form text-center">
                        <form action="#" id="konfirmasi">
                            <div class="row mrg20">
                                <div class="col-md-12 col-sm-12 col-lg-12">
																																<input type="email" v-model="email" placeholder="Email">     
                                </div>
																																<div class="col-md-12 col-sm-12 col-lg-12">
																																<input type="password" v-model="password" placeholder="Password">     
                                </div>
                                
																																<div class="col-md-12 col-sm-12 col-lg-12">
																																<img
																																		:src="imgCaptcha"
																																		alt=""
																																>
																																<input
																																		v-model="captcha"
																																		placeholder="Koda Captcha. . . . . . . . . . . . . . . . . ."
																																		type="text"
																																		class="form-control mt-2"
																																>
                                </div>
																																<div class="col-md-12 col-sm-12 col-lg-12">
																																
																																				<br><br>
                                    <a class="thm-btn brd-rd40" href="#" @click="checkForm()">LOGIN</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
// $("#files").change(function() {
//   filename = this.files[0].name
//   console.log(filename);
// });

var konfirmasi = new Vue({
	el:"#konfirmasi",
	data: {
			email:'',
			password:'',
			captcha:'',
			imgCaptcha:'',
			key:'',
	},
	mounted() {
		this.refreshCaptcha()
	},
	methods: {
		async refreshCaptcha () {
			let urlcaptcha = "<?php echo url('api/get-captcha/flat'); ?>";
					await axios.get(urlcaptcha).then((response) =>{
							this.imgCaptcha = response.data.img
							this.key = response.data.key
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});      
    },
		loading(text){
			Swal.fire({
									title: 'Mohon menunggu...',
									text:text,
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
		},
		async checkForm() {
				this.loading('Sedang dikirim...');
				if (this.email < 3) {
						swal("Opss...", "Email Wajib diisi.", "error");
						Swal.close()
				}else if(this.password < 6) {
						swal("Opss...", "Password minimal 6 Digit ", "error");
						Swal.close()
				}else{
					this.refreshCaptcha()
					let url = "<?php echo route('masuk'); ?>";
					let request = {email:this.email, password: this.password, captcha : this.captcha, key: this.key};
					request['token'] = document.querySelector('#token').getAttribute('value');
					await axios.post(url,request).then((response) =>{
							swal(response.data.title, response.data.message, response.data.icon);
							if (response.data.code === 200) {
								window.location.href = "<?php echo route('home'); ?>";
							}
							Swal.close()									
					},(response)=>{
									swal("Gagal!", "GAGAL", "error");
									Swal.close()
					});
				}
		},
	}
});
</script>
@endsection